package com.example.demo.Entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "donhang")
public class donhang implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "dhid")
	private int dhid;

	@Column(name = "tongtien")
	private int tongtien;

	@ManyToOne
	@JoinColumn(name = "userid")
	private users users;
	
	@Column(name = "trangthai")
	private String trangthai;
	
	@Column(name = "ngaydat")
	private Date ngaydat = new Date();
	
	@OneToMany(fetch = FetchType.LAZY,mappedBy = "donhang")
//  @JsonIgnore
	private List<dh_sp> dh_sp;
}