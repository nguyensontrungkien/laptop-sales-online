package com.example.demo.Entity;

import java.io.Serializable;
import java.util.List;


import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "thuonghieu")
public class thuonghieu implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@Column(name = "thid")
	private String thid;

	@Column(name = "tenth")
	private String tenth;
	
	@Column(name = "logo")
	private String logo;
	
	@OneToMany(fetch = FetchType.LAZY,mappedBy = "thuonghieu")
//  @JsonIgnore
	private List<sanpham> sanpham;
}
