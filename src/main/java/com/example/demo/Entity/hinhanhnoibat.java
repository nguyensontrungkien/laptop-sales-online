package com.example.demo.Entity;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "hinhanhnoibat")
public class hinhanhnoibat implements Serializable{
	@Id
	@Column(name = "hinhid")
	private int hinhid;
	
	@Column(name = "url")
	private String url;
	
	@ManyToOne
	@JoinColumn(name = "spid")
	private sanpham sanpham;
}
