package com.example.demo.Entity;

import java.util.Date;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "users")
public class users {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "userid")
	private int userid;
	
	@Column(name = "username")
	private String username;
	@Column(name = "password")
	private String password;
	@Column(name = "sdt")
	private String sdt;
	@Column(name = "email")
	private String email;
	@Column(name = "diachi")
	private String diachi;
	@Column(name = "ngaytao")
	private Date ngaytao = new Date();
	@Column(name = "role")
	private boolean role;
	@Column(name = "image")
	private String image;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "users")
	List<giohang> giohang;
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "users")
	List<donhang> donhang;

}
