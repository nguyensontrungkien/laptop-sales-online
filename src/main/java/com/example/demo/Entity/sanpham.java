package com.example.demo.Entity;

import java.io.Serializable;
import java.util.List;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.FetchType;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.OneToMany;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "sanpham")
public class sanpham implements Serializable{
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "spid")
	private int spid;

	@Column(name = "tensp")
	private String tensp;

	@ManyToOne
	@JoinColumn(name = "thid")
	private thuonghieu thuonghieu;

	@Column(name = "gia")
	private int gia;

	@Column(name = "giacu")
	private int giacu;
	
	@Column(name = "hinh")
	private String hinh;

	@Column(name = "mausac")
	private String mausac;

	@Column(name = "cpu")
	private String cpu;

	@Column(name = "sonhan")
	private int sonhan;

	@Column(name = "soluong")
	private int soluong;

	@Column(name = "tocdocpu")
	private String tocdocpu;

	@Column(name = "tocdotoida")
	private String tocdotoida;

	@Column(name = "ram")
	private String ram;

	@Column(name = "loairam")
	private String loairam;

	@Column(name = "tocdobusram")
	private String tocdobusram;

	@Column(name = "hotroramtoida")
	private String hotroramtoida;

	@Column(name = "rom")
	private String rom;

	@Column(name = "manhinh")
	private String manhinh;

	@Column(name = "dophangiai")
	private String dophangiai;

	@Column(name = "tansoquet")
	private String tansoquet;

	@Column(name = "congnghemanhinh")
	private String congnghemanhinh;

	@Column(name = "cardmh")
	private String cardmh;

	@Column(name = "congngheamthanh")
	private String congngheamthanh;

	@Column(name = "tinhnangkhac")
	private String tinhnangkhac;

	@Column(name = "denbanphim")
	private String denbanphim;

	@Column(name = "hdh")
	private String hdh;

	@Column(name = "thongtinpin")
	private String thongtinpin;

	@Column(name = "thietke")
	private String thietke;

	@Column(name = "ngaysx")
	private String ngaysx;

	@Column(name = "kichthuoc")
	private String kichthuoc;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "sanpham")
	List<hinhanh> hinhanh;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "sanpham")
	List<hinhanhnoibat> hinhanhnoibat;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "sanpham")
	List<giohang> giohang;
}
