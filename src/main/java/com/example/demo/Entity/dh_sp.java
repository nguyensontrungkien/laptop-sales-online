package com.example.demo.Entity;

import java.io.Serializable;
import java.util.Date;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "dh_sp")
public class dh_sp implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "madhsp")
	private int madhsp;

	@Column(name = "ngaydat")
	private Date ngaydat = new Date();

	@ManyToOne
	@JoinColumn(name = "spid")
	private sanpham sanpham;

	@ManyToOne
	@JoinColumn(name = "dhid")
	private donhang donhang;
}
