package com.example.demo.Entity;

import java.io.Serializable;

import jakarta.persistence.Column;
import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.GenerationType;
import jakarta.persistence.Id;
import jakarta.persistence.JoinColumn;
import jakarta.persistence.ManyToOne;
import jakarta.persistence.Table;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Entity
@Data
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "giohang")
public class giohang implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "ghid")
	private int ghid;

	@ManyToOne
	@JoinColumn(name = "spid")
	private sanpham sanpham;
	
	@ManyToOne
	@JoinColumn(name = "userid")
	private users users;
	
	@Column(name = "soluong")
	private int soluong;
	@Column(name = "tong")
	private int tong;
	
}
