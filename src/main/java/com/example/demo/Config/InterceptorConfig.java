package com.example.demo.Config;

import org.apache.taglibs.standard.lang.jstl.test.beans.PublicBean1;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;

import com.example.demo.Service.AuthInterceptor;
import com.example.demo.Service.SessionService;

@Configuration
public class InterceptorConfig implements WebMvcConfigurer {

//	@Autowired
//	GlobalInterceptor global;

	@Autowired
	AuthInterceptor auth;
	@Autowired
	SessionService session;

	@Override
	public void addInterceptors(InterceptorRegistry registry) {
		// TODO Auto-generated method stub
//		registry.addInterceptor(global)
//			.addPathPatterns("/**")
//			.excludePathPatterns("/assets/**");
		registry.addInterceptor(auth)
			.addPathPatterns("/qlsp/**", "/qlth/**","/cart/**","/donhang/**","/category/**","/qlsp/**", "/qlth/**")
			.excludePathPatterns("/trangchu/", "/ctsp/", "/login/**","/register/**");
		// .excludePathPatterns("/assets/**");
	}
}
