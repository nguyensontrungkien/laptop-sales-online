package com.example.demo.Repository;

import java.awt.print.Pageable;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.Entity.users;

@Repository
public interface UsersRepository extends JpaRepository<users, Integer>{
	@Query("select user from users user where user.email like ?1 and user.password like ?2 ")
	users findByCheckUser(String email, String password);
	@Modifying
	@Query("update users user set user.username=:username,user.email =:email,user.sdt=:sdt,user.diachi=:diachi where user.userid =:userid")
	void updateuser(@Param("username") String username,@Param("email") String email,@Param("sdt") String sdt,@Param("diachi") String diachi,@Param("userid") int userid);
	@Modifying
	@Query("update users user set user.password=:password where user.userid =:userid")
	void updatemk(@Param("password") String password,@Param("userid") int userid);
	
	@Modifying
	@Query("update users user set user.image=:image where user.userid =:userid")
	void updateimage(@Param("image") String image,@Param("userid") int userid);
	
	@Query( "SELECT user FROM users user WHERE user.role =:role")
	Page<users> findByRole(@Param("role") boolean role, PageRequest pageable);
}
