package com.example.demo.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.Entity.dh_sp;
import com.example.demo.Entity.donhang;



public interface Dh_spRepository extends JpaRepository<dh_sp, Integer>{
	@Query("select dh_sp from dh_sp dh_sp where dh_sp.donhang.dhid =:dhid")
	List<dh_sp> findDh_spByDhId(@Param("dhid") int dhid);
	@Query("select count(dh_sp) from dh_sp dh_sp where dh_sp.donhang.dhid =:dhid")
	int count(@Param("dhid") int dhid);
}