package com.example.demo.Repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.Entity.donhang;
import com.example.demo.Entity.sanpham;

public interface DonhangRepository extends JpaRepository<donhang, Integer> {
	@Query("select dh from donhang dh where dh.users.userid =:userid and dh.trangthai =:trangthai")
	Page<donhang> findDhByUserId(@Param("userid") int userid, @Param("trangthai") String trangthai,
			PageRequest pageableSp);
}
