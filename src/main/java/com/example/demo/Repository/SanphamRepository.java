package com.example.demo.Repository;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.Entity.sanpham;

public interface SanphamRepository extends JpaRepository<sanpham, Integer> {
	@Query("select sp from sanpham sp where sp.tensp ilike %?1% or sp.cpu ilike %?1% or sp.thuonghieu.thid ilike %?1% ")
	List<sanpham> findBySearch(String tensp);
	
	@Query("select sp from sanpham sp where sp.spid in (:list) ")
	List<sanpham> findManySpid(@Param("list") List<Integer> list);
	
	@Query("select sp from sanpham sp where sp.thuonghieu.thid in (:th) and sp.mausac in (:mau) or sp.thuonghieu.thid in (:th) or sp.mausac in (:mau)")
	List<sanpham> performSearch(@Param("th") String[] th,@Param("mau") String[] mau);
}
