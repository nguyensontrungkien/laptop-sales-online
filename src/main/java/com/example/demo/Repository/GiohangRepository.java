package com.example.demo.Repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.example.demo.Entity.giohang;
import com.example.demo.Entity.sanpham;

@Repository
public interface GiohangRepository  extends JpaRepository<giohang, Integer>{
	@Query("select gh from giohang gh where gh.users.userid  =:userid")
	List<giohang> findGh(@Param("userid") int userid);
	
	@Query("select gh from giohang gh where gh.sanpham.spid  =:spid")
	giohang findSpid(@Param("spid") int spid);
	
	@Modifying
	@Query("update giohang gh set gh.soluong  =:soluong , gh.tong =:tong where gh.ghid =:ghid")
	void updateQuantity(@Param("soluong") int soluong,@Param("tong") int tong,@Param("ghid") int ghid);
	
	@Query("select gh from giohang gh where gh.ghid in (:list) ")
	List<giohang> findManyGhid(@Param("list") List<Integer> list);
//	
//	@Query("select gh from giohang gh where gh.users.userid =:userid ")
//	List<giohang> findAllByUserId(@Param("userid") int userid);
}
