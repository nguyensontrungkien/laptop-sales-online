package com.example.demo.Controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.Entity.dh_sp;
import com.example.demo.Entity.donhang;
import com.example.demo.Entity.giohang;
import com.example.demo.Entity.sanpham;
import com.example.demo.Entity.users;
import com.example.demo.Repository.Dh_spRepository;
import com.example.demo.Repository.DonhangRepository;
import com.example.demo.Repository.GiohangRepository;
import com.example.demo.Repository.SanphamRepository;
import com.example.demo.Repository.UsersRepository;
import com.example.demo.Service.CookieService;
import com.example.demo.Service.PathService;
import com.example.demo.Service.SessionService;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.transaction.Transactional;
import jakarta.websocket.Session;

@Controller
public class GiohangController {
	@Autowired
	HttpServletRequest req;

	@Autowired
	GiohangRepository repo;

	@Autowired
	UsersRepository repouser;

	@Autowired
	SanphamRepository reposp;

	@Autowired
	CookieService cookie;

	@Autowired
	PathService path;
	
	@Autowired
	SessionService session;
	private Map<Integer, Integer> cartItems = new HashMap<>();;

	@RequestMapping("/cart/")
	public String cart(Model model) {
		cookie.assignToSession(model);
		users user = session.get("user");
		req.setAttribute("result", path.path());
		List<giohang> gh = (List<giohang>) repo.findGh(user.getUserid());
		model.addAttribute("gh", gh);
		return "layout/index";
	}

	@RequestMapping("/cart/{spid}")
	public String save(@PathVariable("spid") int spid, Model model) {
		users user = session.get("user");
		req.setAttribute("result", path.path());
		int soluong = Integer.valueOf( req.getParameter("soluong"));
		sanpham sp = reposp.findById(spid).get();
		giohang ghsave = new giohang();
		if (cartItems.containsKey(sp.getSpid())) {
			int currentQuantity = cartItems.get(sp.getSpid());
			cartItems.put(sp.getSpid(), currentQuantity + soluong);
		} else {
			cartItems.put(sp.getSpid(), soluong);
		}
		for (Map.Entry<Integer, Integer> entry : cartItems.entrySet()) {
			int spid2 = entry.getKey();
			int quantity = entry.getValue();
			giohang cartEntity = repo.findSpid(spid2);
			if (cartEntity != null) {
				cartEntity.setSoluong(quantity);
				cartEntity.setTong(quantity*cartEntity.getSanpham().getGia());
				repo.save(cartEntity);
			} else {
				cartEntity = new giohang(0, sp, user, quantity, quantity * sp.getGia());
				repo.save(cartEntity);
			}
		}

		return "redirect:/cart/";
	}

	@RequestMapping("/cart/delete/{ghid}")
	public String delete(@PathVariable("ghid") int ghid, Model model) {
		req.setAttribute("result", path.path());
		repo.deleteById(ghid);
		List<giohang> gh = repo.findAll();
		model.addAttribute("gh", gh);
		return "redirect:/cart/";
	}

	@Transactional
	@PostMapping("/cart/update/{ghid}")
	public String update(@PathVariable("ghid") int ghid, Model model, giohang gh1, RedirectAttributes params) {
		req.setAttribute("result", path.path());
		giohang gh = repo.findById(ghid).get();
		repo.updateQuantity(gh1.getSoluong(), gh.getSanpham().getGia() * gh1.getSoluong(), ghid);
		return "redirect:/cart/";
	}


}
