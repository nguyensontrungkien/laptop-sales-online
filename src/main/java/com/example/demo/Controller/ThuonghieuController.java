package com.example.demo.Controller;

import java.util.Iterator;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.Entity.thuonghieu;
import com.example.demo.Repository.ThuonghieuRepository;
import com.example.demo.Service.PathService;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.websocket.server.PathParam;

@Controller
public class ThuonghieuController {
	@Autowired
	ThuonghieuRepository repo;

	@Autowired
	PathService path;

	@Autowired
	HttpServletRequest req;

	@RequestMapping("/category/")
	public String category(Model model) {
		req.setAttribute("result", path.path());
		List<thuonghieu> th = repo.findAll();
		model.addAttribute("th", th);
		return "layout/index";
	}
	
	@RequestMapping("/category/{thid}")
	public String category(Model model,@PathVariable("thid") String thid ) {
		req.setAttribute("result", path.path());
		thuonghieu thsp = (thuonghieu) repo.findById(thid).get();
		model.addAttribute("thsp", thsp);
		List<thuonghieu> th = repo.findAll();
		model.addAttribute("th", th);
		return "layout/index";
	}
	@RequestMapping("/qlth/insert")
	public String categoryInsert(Model model, thuonghieu th) {
		req.setAttribute("result", path.path());
		repo.save(th);
		return "redirect:/qlth/";
	}
	
	@RequestMapping("/qlth/edit/{thid}")
	public String categoryEdit(Model model,@PathVariable("thid") String thid, thuonghieu th,RedirectAttributes params) {
		req.setAttribute("result", path.path());
		thuonghieu th1 = repo.findById(thid).get();
		params.addFlashAttribute("th1", th1);	
		return "redirect:/qlth/";
	}
	@RequestMapping("/qlth/update")
	public String categoryUpdate(Model model, thuonghieu th) {
		req.setAttribute("result", path.path());
		repo.save(th);
		return "redirect:/qlth/";
	}
	@RequestMapping("/qlth/delete/{thid}")
	public String categoryDelete(Model model,@PathVariable("thid") String thid) {
		req.setAttribute("result", path.path());
		repo.deleteById(thid);
		return "redirect:/qlth/";
	}
	@RequestMapping("/qlth/clean")
	public String categoryClean(Model model,@PathVariable("check") String check) {
		req.setAttribute("result", path.path());
		return "redirect:/qlth/";
	}
	@RequestMapping("/qlth/")
	public String qlth(Model model,@RequestParam("p") Optional<Integer> p) {
		req.setAttribute("result", path.path());
		PageRequest pageableTh = PageRequest.of(p.orElse(0), 5);
		Page<thuonghieu> pageTh = repo.findAll(pageableTh);
		model.addAttribute("th", pageTh);
		return "FormAdmin/index";
	}
}
