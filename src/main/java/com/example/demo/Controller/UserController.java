package com.example.demo.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.Entity.thuonghieu;
import com.example.demo.Entity.users;
import com.example.demo.Repository.ThuonghieuRepository;
import com.example.demo.Repository.UsersRepository;
import com.example.demo.Service.PathService;

import jakarta.servlet.http.HttpServletRequest;

@Controller
public class UserController {
	@Autowired
	ThuonghieuRepository repo;

	@Autowired
	PathService path;

	@Autowired
	HttpServletRequest req;
	
	@Autowired
	UsersRepository repouser;

	@RequestMapping("/qluser/insert")
	public String categoryInsert(Model model, users user) {
		req.setAttribute("result", path.path());
		users user1 = new users();
		user1.setUsername(user.getUsername());
		user1.setPassword(user.getPassword());
		user1.setSdt(user.getSdt());
		user1.setEmail(user.getEmail());
		user1.setDiachi(user.getDiachi());
		user1.setImage("pp.png");
		user1.setRole(false);

		
		repouser.save(user1);
		return "redirect:/qluser/";
	}

	@RequestMapping("/qluser/edit/{userid}")
	public String categoryEdit(Model model, @PathVariable("userid") int userid, users user,
			RedirectAttributes params) {
		req.setAttribute("result", path.path());
		users user1 = repouser.findById(userid).get();
		params.addFlashAttribute("user1", user1);
		return "redirect:/qluser/";
	}

//	@RequestMapping("/qlth/update")
//	public String categoryUpdate(Model model, thuonghieu th) {
//		req.setAttribute("result", path.path());
//		repo.save(th);
//		return "redirect:/qlth/";
//	}
//
//	@RequestMapping("/qlth/delete/{thid}")
//	public String categoryDelete(Model model, @PathVariable("thid") String thid) {
//		req.setAttribute("result", path.path());
//		repo.deleteById(thid);
//		return "redirect:/qlth/";
//	}
//
//	@RequestMapping("/qlth/clean")
//	public String categoryClean(Model model, @PathVariable("check") String check) {
//		req.setAttribute("result", path.path());
//		return "redirect:/qlth/";
//	}

	@RequestMapping("/qluser/")
	public String qlth(Model model, @RequestParam("p") Optional<Integer> p) {
		req.setAttribute("result", path.path());
		PageRequest pageableTh = PageRequest.of(p.orElse(0), 5);
		boolean role = false;
		Page<users> user = repouser.findByRole(role, pageableTh);
		model.addAttribute("user", user);
		return "FormAdmin/index";
	}
	

}
