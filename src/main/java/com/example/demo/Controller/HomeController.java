package com.example.demo.Controller;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;
import com.example.demo.Entity.sanpham;
import com.example.demo.Entity.thuonghieu;
import com.example.demo.Entity.users;
import com.example.demo.Repository.SanphamRepository;
import com.example.demo.Repository.ThuonghieuRepository;
import com.example.demo.Repository.UsersRepository;
import com.example.demo.Service.CookieService;
import com.example.demo.Service.PathService;
import com.example.demo.Service.SessionService;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;

@Controller
public class HomeController {
	@Autowired
	SanphamRepository repo;

	@Autowired
	ThuonghieuRepository repo2;
	
	@Autowired
	UsersRepository repo3;

	@Autowired
	HttpServletRequest req;

	@Autowired
	PathService path;

	@Autowired
	SessionService session;
	@Autowired
	CookieService cookie;

	@RequestMapping("/trangchu/")
	public String SanphamFillAll(Model model) {
		req.setAttribute("result", path.path());
		List<sanpham> sp = repo.findAll();
		model.addAttribute("sp", sp);
		List<thuonghieu> th = repo2.findAll();
		model.addAttribute("th", th);
		System.out.println(th.get(6).getTenth());
		if (cookie.get("userid") != null) {
			users user = repo3.findById(Integer.valueOf(cookie.getValue("userid"))).get();
			session.set("user", user);
			model.addAttribute("user", session.get("user"));
		} else {
			model.addAttribute("user", session.get("user"));
		}
		return "layout/index";
	}
//	@RequestMapping("/trangchuad/{check}")
//	public String trangchu_admin(Model model,@PathVariable("check") String check ,@RequestParam("p") Optional<Integer> p) {
//		req.setAttribute("result", path.path());
//		model.addAttribute("check", check);
//		PageRequest pageableSp = PageRequest.of(p.orElse(0), 5);
//		Page<sanpham> pageSp = repo.findAll( pageableSp);
//		model.addAttribute("sp", pageSp);
//		//thương hiệu
//		thuonghieu th= (thuonghieu) model.getAttribute("th");
//		model.addAttribute("th1",th);	
////		System.out.println(th.getThid());
//		PageRequest pageableTh = PageRequest.of(p.orElse(0), 5);
//		Page<thuonghieu> pageTh = repo2.findAll( pageableTh);
//		model.addAttribute("th2", pageTh);
//		return "layout/index";
//	}
}
