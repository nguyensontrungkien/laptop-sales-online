package com.example.demo.Controller;

import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Optional;

import org.eclipse.tags.shaded.org.apache.xpath.operations.Mod;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.Entity.dh_sp;
import com.example.demo.Entity.donhang;
import com.example.demo.Entity.sanpham;
import com.example.demo.Entity.users;
import com.example.demo.Repository.Dh_spRepository;
import com.example.demo.Repository.DonhangRepository;
import com.example.demo.Repository.UsersRepository;
import com.example.demo.Service.CookieService;
import com.example.demo.Service.PathService;
import com.example.demo.Service.SessionService;

import jakarta.servlet.http.HttpServletRequest;
import jakarta.transaction.Transactional;
import jakarta.websocket.server.PathParam;

@Controller
@CrossOrigin("*")
public class ProfileController {

	@Autowired
	HttpServletRequest req;

	@Autowired
	PathService path;

	@Autowired
	CookieService cookie;

	@Autowired
	SessionService session;

	@Autowired
	DonhangRepository repodh;
	@Autowired
	Dh_spRepository repodh_sp;
	@Autowired
	UsersRepository repouser;

	@RequestMapping("/profile/")
	public String profile(Model model) {
		req.setAttribute("result", path.path());
		cookie.assignToSession(model);
		users users = session.get("user");
		users user = repouser.findById(users.getUserid()).get();
		model.addAttribute("user", user);
		
		return "layout/index";
	}

	@GetMapping("/ctdh/")
	public String view() {
		req.setAttribute("result", path.path());
		return "layout/index";
	}
	
	@GetMapping("/ctdh/dangxuli/")
	public String dh(Model model, @RequestParam("p") Optional<Integer> p, RedirectAttributes params) {
		req.setAttribute("result", path.path());
		cookie.assignToSession(model);
		model.addAttribute("user", session.get("user"));
		users user = session.get("user");
		PageRequest pageableSp = PageRequest.of(p.orElse(0), 4);
		Page<donhang> dh = (Page<donhang>) repodh.findDhByUserId(user.getUserid(), "đang xử lí", pageableSp);
		params.addFlashAttribute("dh", dh);
		return "redirect:/ctdh/";
	}

	@GetMapping("/ctdh/dagiao")
	public String ctdh_dagiao(Model model, @RequestParam("p") Optional<Integer> p, RedirectAttributes params) {
		req.setAttribute("result", path.path());
		cookie.assignToSession(model);
		model.addAttribute("user", session.get("user"));
		users user = session.get("user");
		PageRequest pageableSp = PageRequest.of(p.orElse(0), 5);
		Page<donhang> dh = (Page<donhang>) repodh.findDhByUserId(user.getUserid(), "đã giao", pageableSp);
		params.addFlashAttribute("dh", dh);
		return "redirect:/ctdh/";
	}

	@GetMapping("/ctdh/dahuy")
	public String ctdh_daghuy(Model model, @RequestParam("p") Optional<Integer> p, RedirectAttributes params) {
		req.setAttribute("result", path.path());
		cookie.assignToSession(model);
		model.addAttribute("user", session.get("user"));
		users user = session.get("user");
		PageRequest pageableSp = PageRequest.of(p.orElse(0), 5);
		Page<donhang> dh = (Page<donhang>) repodh.findDhByUserId(user.getUserid(), "đã hủy", pageableSp);
		params.addFlashAttribute("dh", dh);
		return "redirect:/ctdh/";
	}

	@GetMapping("/ctdh/{dhid}")
	public String ctdh(Model model, @PathVariable("dhid") int dhid, RedirectAttributes params,
			@RequestParam("p") Optional<Integer> p) {
		req.setAttribute("result", path.path());
		params.addFlashAttribute("check", String.valueOf(dhid));
		cookie.assignToSession(model);
		model.addAttribute("user", session.get("user"));
		users user = session.get("user");
		PageRequest pageableSp = PageRequest.of(p.orElse(0), 5);
		Page<donhang> dh = repodh.findDhByUserId(user.getUserid(), "đang xử lí", pageableSp);
		List<dh_sp> dhsp = repodh_sp.findDh_spByDhId(dhid);
		int count = repodh_sp.count(dhid);
		params.addFlashAttribute("dh", dh);
		params.addFlashAttribute("dhsp", dhsp);
		params.addFlashAttribute("count", count);
		return "redirect:/ctdh/";
	}

	@GetMapping("/ctdh/dagiao/{dhid}")
	public String ctdh_dagiao(Model model, @PathVariable("dhid") int dhid, RedirectAttributes params,
			@RequestParam("p") Optional<Integer> p) {
		req.setAttribute("result", path.path());
		params.addFlashAttribute("check", String.valueOf(dhid));
		cookie.assignToSession(model);
		model.addAttribute("user", session.get("user"));
		users user = session.get("user");
		PageRequest pageableSp = PageRequest.of(p.orElse(0), 5);
		Page<donhang> dh = repodh.findDhByUserId(user.getUserid(), "đang đã giao", pageableSp);
		List<dh_sp> dhsp = repodh_sp.findDh_spByDhId(dhid);
		int count = repodh_sp.count(dhid);
		params.addFlashAttribute("dhsp", dhsp);
		params.addFlashAttribute("count", count);
		return "redirect:/ctdh/";
	}

	@GetMapping("/ctdh/dahuy/{dhid}")
	public String ctdh_dahuy(Model model, @PathVariable("dhid") int dhid, RedirectAttributes params,
			@RequestParam("p") Optional<Integer> p) {
		req.setAttribute("result", path.path());
		params.addFlashAttribute("check", String.valueOf(dhid));
		cookie.assignToSession(model);
		model.addAttribute("user", session.get("user"));
		users user = session.get("user");
		PageRequest pageableSp = PageRequest.of(p.orElse(0), 5);
		Page<donhang> dh = repodh.findDhByUserId(user.getUserid(), "đã hủy", pageableSp);
		List<dh_sp> dhsp = repodh_sp.findDh_spByDhId(dhid);
		int count = repodh_sp.count(dhid);
		
		params.addFlashAttribute("dhsp", dhsp);
		params.addFlashAttribute("count", count);
		return "redirect:/ctdh/";
	}
	@Transactional
	@PostMapping("/profile/updatemk/")
	public String Updatemk(Model model, RedirectAttributes params) {
		cookie.assignToSession(model);
		users user = session.get("user");
		String mkc = req.getParameter("mkc");
		String mkm = req.getParameter("mkm");
		String xnmk = req.getParameter("xnmk");
		repouser.updatemk(xnmk,user.getUserid());
		return "redirect:/profile/";
	}
	
	@Transactional
	@PostMapping("/profile/information/")
	public String profileInformation(Model model, RedirectAttributes params, users user) {
		cookie.assignToSession(model);
		users userss = session.get("user");
		repouser.updateuser(user.getUsername(),user.getEmail(),user.getSdt(),user.getDiachi(),userss.getUserid());
		return "redirect:/profile/";
	}
	@Transactional
	@PostMapping("/profile/updateimage/")
	public String profileimage(Model model, RedirectAttributes params, users user,@RequestParam("attach") MultipartFile attach) throws IllegalStateException, IOException {
		cookie.assignToSession(model);
		users userss = session.get("user");
		if (!attach.isEmpty()) {
			String filename = attach.getOriginalFilename();
			String uploadDir = "C:/Users/kien/Documents/workspace-spring-tool-suite-4-4.16.1.RELEASE/ASM_JAVA5/src/main/resources/static/images/"; // Đường dẫn tuyệt đối đến thư mục docs trên hệ thống tệp của bạn
			File file = new File(uploadDir + filename);
			attach.transferTo(file);
			System.out.println(filename);
			repouser.updateimage(filename, userss.getUserid());
		}
		
		return "redirect:/profile/";
	}
}
