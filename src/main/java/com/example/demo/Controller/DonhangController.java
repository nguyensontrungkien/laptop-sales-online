package com.example.demo.Controller;

import java.io.File;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.jasper.tagplugins.jstl.core.Url;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

import com.example.demo.Entity.MailInfo;
import com.example.demo.Entity.dh_sp;
import com.example.demo.Entity.donhang;
import com.example.demo.Entity.giohang;
import com.example.demo.Entity.sanpham;
import com.example.demo.Entity.users;
import com.example.demo.Helper.MailerHelper;
import com.example.demo.Repository.Dh_spRepository;
import com.example.demo.Repository.DonhangRepository;
import com.example.demo.Repository.GiohangRepository;
import com.example.demo.Repository.SanphamRepository;
import com.example.demo.Repository.UsersRepository;
import com.example.demo.Service.CookieService;
import com.example.demo.Service.EmailService;
//import com.example.demo.Service.MailerService;
//import com.example.demo.Service.MailerServiceImp;
import com.example.demo.Service.PathService;
import com.example.demo.Service.SessionService;

import jakarta.servlet.http.HttpServletRequest;

@Controller
public class DonhangController {
	@Autowired
	HttpServletRequest req;

	@Autowired
	GiohangRepository repo;

	@Autowired
	Dh_spRepository repodh_sp;

	@Autowired
	DonhangRepository repodh;

	@Autowired
	PathService path;

	@Autowired
	CookieService cookie;

	@Autowired
	UsersRepository repouser;

	@Autowired
	SessionService session;

	@Autowired
	SanphamRepository reposp;

	// @Autowired
	// MailerServiceImp mailer;

	@Autowired
	private EmailService emailService;

	@RequestMapping("/bill/")
	public String view(@RequestParam("check") List<Integer> checkid, Model model) {
		req.setAttribute("result", path.path());
		LocalDate currentDate = LocalDate.now();
		List<giohang> gh = repo.findManyGhid(checkid);
		model.addAttribute("gh", gh);
		int formattedAmount = 0;
		for (giohang giohang : gh) {
			formattedAmount += giohang.getTong();
		}
		DecimalFormat decimalFormat = new DecimalFormat("#,###₫");
        String tong = decimalFormat.format(formattedAmount);
		model.addAttribute("total", tong);
		model.addAttribute("ngaydat", currentDate);
		session.set("ghid", gh);
		return "layout/index";
	}

	@RequestMapping("/bill2/{spid}")
	public String view_sp(@PathVariable("spid") int spid, Model model) {
		req.setAttribute("result", path.path());
		users user = session.get("user");
		int soluong = Integer.valueOf(req.getParameter("soluong"));
		LocalDate currentDate = LocalDate.now();
		sanpham sp = reposp.findById(spid).get();
		model.addAttribute("sp", sp);
		int formattedAmount = sp.getGia() * soluong;
		DecimalFormat decimalFormat = new DecimalFormat("#,###₫");
        String tong = decimalFormat.format(formattedAmount);
		model.addAttribute("total", tong);
		model.addAttribute("ngaydat", currentDate);
		model.addAttribute("user", user);
		session.set("tong", tong);
		session.set("soluong", soluong);
		return "layout/index";
	}

	@GetMapping("/donhang/ok/{spid}")
	public String total2(@PathVariable("spid") int spid, Model model, MailInfo mail) throws ParseException {
		req.setAttribute("result", path.path());
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		cookie.assignToSession(model);
		users user = session.get("user");
		int soluong = session.get("soluong");
		int tong = session.get("tong");
		donhang dh = new donhang();
		dh.setTongtien(tong);
		dh.setUsers(user);
		dh.setTrangthai("đang xử lí");
		repodh.save(dh);
		donhang dh2 = repodh.findById(dh.getDhid()).get();

		sanpham sp = reposp.findById(spid).get();
		dh_sp dhsp = new dh_sp();
		dhsp.setMadhsp(1);
		dhsp.setSanpham(sp);
		dhsp.setDonhang(dh2);
		repodh_sp.save(dhsp);

		StringBuilder contentBuilder = new StringBuilder();
		
		contentBuilder.append("<div style=\"width: 800px;height: auto;background-color: rgb(145, 213, 189)\">");
		contentBuilder.append("<h1 style=\"color:blue;text-align: center;\">Danh sách sản phẩm bạn đã thanh toán</h1>");
		contentBuilder.append(
				"<div style=\"border: 2px solid black;background-color: rgb(167, 239, 245);margin-bottom: 10px;border-radius: 10px;\">");
		contentBuilder.append("<strong>Name: </strong> ").append(sp.getTensp()).append("<br>");
		contentBuilder.append("<strong>Số lượng: </strong> ").append(soluong).append("<br>");
		contentBuilder.append("<strong>Tổng giá sản phẩm: </strong> ").append(sp.getGia()*soluong);
		contentBuilder.append("<img src='/src/main/resources/static/images/").append(sp.getHinh())
				.append("' alt='").append(sp.getHinh()).append("'>");
		contentBuilder.append("</div>");
		contentBuilder.append("</div>");
		
		try {
			emailService.sendEmail("nguyenthanhphat08012003@gmail.com", "XÁC NHẬN ĐƠN HÀNG", contentBuilder.toString());
		} catch (Exception e) {
		}
		return "redirect:/trangchu/";
	}

	@GetMapping("/donhang/ok/")
	public String total(Model model, MailInfo mail) throws ParseException {
		req.setAttribute("result", path.path());
		SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
		cookie.assignToSession(model);
		users user = session.get("user");
		List<giohang> gh = session.get("ghid");
		int tong = 0;
		for (giohang giohang : gh) {
			tong += giohang.getTong();
		}
		donhang dh = new donhang();
		dh.setTongtien(tong);
		dh.setUsers(session.get("user"));
		dh.setTrangthai("đang xử lí");
		repodh.save(dh);
		donhang dh2 = repodh.findById(dh.getDhid()).get();

		for (giohang productId : gh) {
			System.out.println(productId.getSanpham().getSpid());
			sanpham sp = reposp.findById(productId.getSanpham().getSpid()).get();
			dh_sp dhsp = new dh_sp();
			dhsp.setMadhsp(1);
			dhsp.setSanpham(sp);
			dhsp.setDonhang(dh2);
			repodh_sp.save(dhsp);
		}
		StringBuilder contentBuilder = new StringBuilder();
		contentBuilder.append("<div style=\"width: 800px;height: auto;background-color: rgb(145, 213, 189)\">");
		contentBuilder.append("<h1 style=\"color:blue;text-align: center;\">Danh sách sản phẩm bạn đã thanh toán</h1>");

		for (giohang gh1 : gh) {
			contentBuilder.append(
					"<div style=\"border: 2px solid black;background-color: white;margin-bottom: 10px;border-radius: 10px; padding:10px;margin-left:10px;margin-right:10px\">");
			contentBuilder.append("<strong>Name: </strong> ").append(gh1.getSanpham().getTensp()).append("<br>");
			contentBuilder.append("<strong>Số lượng: </strong> ").append(gh1.getSoluong()).append("<br>");
			contentBuilder.append("<strong>Tổng giá sản phẩm: </strong> ").append(gh1.getTong());
			contentBuilder.append("<img src='/src/main/resources/static/images/").append(gh1.getSanpham().getHinh())
					.append("' alt='").append(gh1.getSanpham().getHinh()).append("'>");
			contentBuilder.append("</div>");
		}
		contentBuilder.append("<strong>Tổng tiền tất cả: </strong> ").append(dh.getTongtien());
		contentBuilder.append("</div>");
		try {
			emailService.sendEmail("nguyenthanhphat08012003@gmail.com", "XÁC NHẬN ĐƠN HÀNG", contentBuilder.toString());
		} catch (Exception e) {
		}
		return "redirect:/trangchu/";
	}

}
