package com.example.demo.Controller;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import org.eclipse.jdt.internal.compiler.apt.model.ModuleElementImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.Entity.thuonghieu;
import com.example.demo.Entity.sanpham;
import com.example.demo.Repository.SanphamRepository;
import com.example.demo.Repository.ThuonghieuRepository;
import com.example.demo.Service.PathService;

import jakarta.servlet.http.HttpServletRequest;

@Controller
public class SanphamController {
	@Autowired
	SanphamRepository repo;

	@Autowired
	ThuonghieuRepository repo2;

	@Autowired
	HttpServletRequest req;

	@Autowired
	PathService path;

	@RequestMapping("/ctsp/{spid}")
	public String ctsp(@PathVariable("spid") Integer spid) {
		req.setAttribute("result", path.path());
		sanpham sp = repo.findById(spid).get();
		req.setAttribute("sp", sp);
		req.setAttribute("h", sp.getHinhanhnoibat());
		List<sanpham> spk = repo.findAll();
		req.setAttribute("spk", spk);
		return "layout/index";
	}

	@RequestMapping("/search/")
	public String search(Model model) {
		req.setAttribute("result", path.path());
		return "layout/index";
	}

	@RequestMapping("/search/findsearch/")
	public String findSearch( Model model, RedirectAttributes params) {
		req.setAttribute("result", path.path());
		String[] th = req.getParameterValues("th");
		String[] mau = req.getParameterValues("mau");
		List<sanpham> sp = repo.performSearch(th,mau);
		params.addFlashAttribute("sp", sp);
		// Trả về view hoặc redirect tùy theo logic của bạn
		return "redirect:/search/";
	}

	@RequestMapping("/search/ok/")
	public String searchkey(Model model, RedirectAttributes params, @RequestParam("p") Optional<Integer> p) {
		req.setAttribute("result", path.path());
		PageRequest pageableSp = PageRequest.of(p.orElse(0), 6);
		String search = req.getParameter("search");
		System.out.println(search);
		List<sanpham> sp = repo.findBySearch(search);
		params.addFlashAttribute("sp", sp);
		return "redirect:/search/";
	}

	@RequestMapping("/qlsp/")
	public String qlsp(Model model, @RequestParam("p") Optional<Integer> p) {
		req.setAttribute("result", path.path());
		PageRequest pageableSp = PageRequest.of(p.orElse(0), 5);
		Page<sanpham> pageSp = repo.findAll(pageableSp);
		model.addAttribute("sp", pageSp);
		List<thuonghieu> th = new ArrayList<>();
		th = repo2.findAll();
		model.addAttribute("th", th);
		return "FormAdmin/index";
	}

	@RequestMapping("/qlsp/insert")
	public String productInsert(Model model, sanpham sp) {
		req.setAttribute("result", path.path());
//		List<sanpham> sp2 = new ArrayList<>();
//		sp2.add(new sanpham());
		repo.save(sp);
		return "redirect:/qlsp/";
	}

	@RequestMapping("/qlsp/edit/{spid}")
	public String productEdit(Model model, @PathVariable("spid") int spid, sanpham sp, RedirectAttributes params) {
		req.setAttribute("result", path.path());
		sanpham sp1 = repo.findById(spid).get();
		params.addFlashAttribute("sp1", sp1);
		return "redirect:/qlsp/";
	}

	@RequestMapping("/qlsp/update")
	public String productUpdate(Model model, sanpham sp) {
		req.setAttribute("result", path.path());
		repo.save(sp);
		return "redirect:/qlsp/";
	}

	@RequestMapping("/qlsp/delete/{spid}")
	public String productDelete(Model model, @PathVariable("spid") int spid) {
		req.setAttribute("result", path.path());
		repo.deleteById(spid);
		return "redirect:/qlsp/";
	}
}
