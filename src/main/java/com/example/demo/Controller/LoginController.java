package com.example.demo.Controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

import com.example.demo.Entity.thuonghieu;
import com.example.demo.Entity.users;
import com.example.demo.Repository.UsersRepository;
import com.example.demo.Service.CookieService;
import com.example.demo.Service.PathService;
import com.example.demo.Service.SessionService;

import jakarta.servlet.http.Cookie;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.websocket.Session;

@Controller
public class LoginController {
	@Autowired
	HttpServletRequest req;
	@Autowired
	PathService path;

	@Autowired
	UsersRepository repo;

	@Autowired
	CookieService cookie;
	
	@Autowired
	SessionService session;

	@RequestMapping("/login/")
	public String Login(Model model) {
		req.setAttribute("result", path.path());
		String email = cookie.getValue("email");
		String password = cookie.getValue("password");
		model.addAttribute("email", email);
		model.addAttribute("password", password);
		return "Login_Register/index";
	}

	@RequestMapping("/register/")
	public String register() {
		req.setAttribute("result", path.path());
		return "Login_Register/index";
	}
	@RequestMapping("/dangxuat/")
	public String dangxuat() {
		req.setAttribute("result", path.path());
		cookie.add("userid", "", 0);
		cookie.add("email", "", 0);
		cookie.add("password", "", 0);
		session.remove("user");
		return "redirect:/trangchu/";
	}

	@RequestMapping("/login/check/")
	public String Login_Check(users user, Model model) {
		req.setAttribute("result", path.path());
		String check = req.getParameter("check");
		users users = new users();
		users = repo.findByCheckUser(user.getEmail(), user.getPassword());
		if (users != null) {
			session.set("user", users);
			if (users.isRole() != true) {
				int hours = (check == null)?0:1*3600;
				cookie.add("email",users.getEmail(), hours);
				cookie.add("password",users.getPassword(), hours);
				cookie.add("userid",String.valueOf(users.getUserid()), hours);
				return "redirect:/trangchu/";
			} else {
				return "redirect:/qlsp/";
			}
		} else {
		}
		return "redirect:/login/";
	}
}
