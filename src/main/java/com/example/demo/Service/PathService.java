package com.example.demo.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import jakarta.servlet.http.HttpServletRequest;

@Service
public class PathService {
	@Autowired
	HttpServletRequest req;

	public String path() {
		req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		String url = req.getRequestURL().toString();
		int thirdSlashIndex = url.indexOf("/", url.indexOf("/") + 2); // Vị trí của dấu / thứ ba
		int fourthSlashIndex = url.indexOf("/", thirdSlashIndex + 2); // Vị trí của dấu / thứ tư
		String result = url.substring(thirdSlashIndex + 1, fourthSlashIndex);
		return result;
	}
	public String path2() {
		req = ((ServletRequestAttributes) RequestContextHolder.getRequestAttributes()).getRequest();
		String url = req.getRequestURL().toString();
		String[] parts = url.split("/");
		String result2 =  parts[parts.length - 2];
		return result2;
	}
}
