package com.example.demo.Service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.servlet.HandlerInterceptor;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.example.demo.Entity.users;

import jakarta.servlet.http.HttpServletResponse;
import jakarta.servlet.http.HttpServletRequest;

@Service
public class AuthInterceptor implements HandlerInterceptor {
	@Autowired
	SessionService session;

	@Override
	public boolean preHandle(HttpServletRequest request, HttpServletResponse response, Object handler)
			throws Exception {
		// TODO Auto-generated method stub
		String uri = request.getRequestURI();
		users user = session.get("user");
		System.out.println(uri);
		String error = "";
		if (user == null) {
			error = "Please login!";
		} else if (user.isRole() == false && uri.startsWith("/qlsp/")) {
			error = "Vui lòng đăng nhập bằng tài khoản Admin";
	
		}
		if (error.length() > 0) {
			session.set("security-uri", uri);
			session.set("alertMessage", error);
			response.sendRedirect("/login/");
			return false;
		}
		return true;
	}
}