package com.example.demo.Service;

import java.io.File;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import jakarta.servlet.http.HttpServletRequest;

@Service
public class ParamService {
	@Autowired
	HttpServletRequest request;

	public String getString(String name, String defaultValue) {
		String value = request.getParameter(name);
		return (value != null) ? value : defaultValue;
	}

    public int getInt(String name, int defaultValue) {
        String value = request.getParameter(name);
        return (value != null) ? Integer.parseInt(value) : defaultValue;
    }

    public double getDouble(String name, double defaultValue) {
        String value = request.getParameter(name);
        return (value != null) ? Double.parseDouble(value) : defaultValue;
    }

	public boolean getBoolean(String name, boolean defaultValue) {
		String value = request.getParameter(name);
		return (value != null) ? Boolean.parseBoolean(value) : defaultValue;
	}

	public Date getDate(String name, String pattern) {
        String value = request.getParameter(name);
        SimpleDateFormat dateFormat = new SimpleDateFormat(pattern);
        try {
            return (value != null) ? dateFormat.parse(value) : null;
        } catch (ParseException e) {
            throw new RuntimeException("Invalid date format", e);
        }
    }

    public File save(MultipartFile file, String path) {
        // Implement your file saving logic here
        return null;
    }
}
