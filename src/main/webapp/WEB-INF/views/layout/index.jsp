<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
	<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
		<!DOCTYPE html>
		<html>

		<head>
			<meta charset="utf-8">
			<title>Insert title here</title>
			<link href="img/favicon.ico" rel="icon">

			<!-- Google Web Fonts -->
			<link rel="preconnect" href="https://fonts.gstatic.com">
			<link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">

			<!-- Font Awesome -->
			<link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">

			<!-- Libraries Stylesheet -->
			<link href="../lib/animate/animate.min.css" rel="stylesheet">
			<link href="../lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
		</head>

		<body>
			<!-- <img src="../TrangForm/trangchu.jsp" alt=""> -->
			<jsp:include page="./header.jsp"></jsp:include>
			<jsp:include page="../TrangForm/${result}.jsp"></jsp:include>
			<jsp:include page="./footer.jsp"></jsp:include>


			<!-- JavaScript Libraries -->
			<script src="https://code.jquery.com/jquery-3.4.1.min.js"></script>
			<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.bundle.min.js"></script>
			<script src="../lib/easing/easing.min.js"></script>
			<script src="../lib/owlcarousel/owl.carousel.min.js"></script>

			<!-- Contact Javascript File -->
			<script src="../mail/jqBootstrapValidation.min.js"></script>
			<script src="../mail/contact.js"></script>

			<!-- Template Javascript -->
			<script src="../js/main.js"></script>
		</body>

		</html>