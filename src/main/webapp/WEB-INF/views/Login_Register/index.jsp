<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <!DOCTYPE html>
        <html>

        <head>
            <meta charset="utf-8">
            <title>Insert title here</title>
            <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css
        " rel="stylesheet">
            <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.bundle.min.js"></script>

            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"></script>
            <link rel="stylesheet"
                href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" />
            <link rel="stylesheet" href="../css/login.css">
        </head>

        <body>
            <div class="container-fluid px-1 px-md-5 px-lg-1 px-xl-5 py-5 mx-auto">
                <div class="card card0 border-0">
                    <div class="row d-flex">
                        <div class="col-lg-6">
                            <div class="card1 pb-5">
                                <div class="row">
                                    <img src="https://i.imgur.com/CXQmsmF.png" class="logo">
                                </div>
                                <div class="row px-3 justify-content-center mt-4 mb-5 border-line">
                                    <img src="https://i.imgur.com/uNGdWHi.png" class="image">
                                </div>
                            </div>
                        </div>
                        <jsp:include page="../Login_Register/${result}.jsp"></jsp:include>
                    </div>
                    <div class="bg-blue py-4">
                        <div class="row px-3">
                            <small class="ml-4 ml-sm-5 mb-2">Copyright &copy; 2019. All rights reserved.</small>
                            <div class="social-contact ml-4 ml-sm-auto">
                                <span class="fa fa-facebook mr-4 text-sm"></span>
                                <span class="fa fa-google-plus mr-4 text-sm"></span>
                                <span class="fa fa-linkedin mr-4 text-sm"></span>
                                <span class="fa fa-twitter mr-4 mr-sm-5 text-sm"></span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </body>

        </html>