<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8">
        <title>Insert title here</title>
    </head>

    <body>
        <div class="col-lg-6">
            <div class="card2 card border-0 px-4 py-5">
                <div class="row mb-4 px-3">
                    <h6 class="mb-0 mr-4 mt-2">Sign in with</h6>
                    <div class="facebook text-center mr-3">
                        <div class="fa fa-facebook"></div>
                    </div>
                    <div class="twitter text-center mr-3">
                        <div class="fa fa-twitter"></div>
                    </div>
                    <div class="linkedin text-center mr-3">
                        <div class="fa fa-linkedin"></div>
                    </div>
                </div>
                <div class="row px-3 mb-4">
                    <div class="line"></div>
                    <small class="or text-center">Or</small>
                    <div class="line"></div>
                </div>
                <form action="/register/" method="post" modelAttribute="user">
                    <div class="row px-3">
                        <label class="mb-1">
                            <h6 class="mb-0 text-sm">Username</h6>
                        </label>
                        <input class="mb-4" type="text" name="username" path="username" placeholder="Enter Username">
                    </div>
                    <div class="row ">
                        <div class="col-6 px-3">
                            <label class="mb-1">
                                <h6 class="mb-0 text-sm">Password</h6>
                            </label>
                            <input type="password" name="password" path="password" placeholder="Enter password">
                        </div>
                        <div class="col-6 px-3">
                            <label class="mb-1">
                                <h6 class="mb-0 text-sm">Phone Number</h6>
                            </label>
                            <input type="text" name="sdt" path="sdt" placeholder="Enter Phone number">
                        </div>
                    </div>
                    <div class="row px-3">
                        <label class="mb-1">
                            <h6 class="mb-0 text-sm">Email Address</h6>
                        </label>
                        <input class="mb-4" type="text" name="email" path="email"
                            placeholder="Enter a valid email address">
                    </div>
                    <div class="row ">
                        <div class="col-6 px-3">
                            <label class="mb-1">
                                <h6 class="mb-0 text-sm">Address</h6>
                            </label>
                            <input type="text" name="diachi" placeholder="Enter Address" path="diachi">
                        </div>
                        <div class="col-6 px-3">
                            <label class="mb-1">
                                <h6 class="mb-0 text-sm">Create Date</h6>
                            </label>
                            <input type="text" name="ngaytao" placeholder="Enter Create date" path="ngaytao">
                        </div>
                    </div>
                    <div class="row px-3">
                        <label class="mb-1">
                            <h6 class="mb-0 text-sm">role</h6>
                        </label>
                        <select class="form-control" name="role" id="" path="role">
                            <option value="0" selected>user</option>
                        </select>
                    </div>
                    <div class="row mb-3 px-3 mt-2">
                        <button type="submit" class="btn btn-blue text-center">Register</button>
                    </div>
                    <div class="row mb-4 px-3">
                        <small class="font-weight-bold">Do you already have an account ? <a href="/login/"
                                class="text-danger ">Login</a></small>
                    </div>
                </form>
            </div>
        </div>
    </body>

    </html>