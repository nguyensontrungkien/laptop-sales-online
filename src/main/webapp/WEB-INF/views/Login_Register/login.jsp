<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8">
        <title>Insert title here</title>
    </head>

    <body>
        <div class="col-lg-6">
            <div class="card2 card border-0 px-4 py-5">
                <div class="row mb-4 px-3">
                    <h6 class="mb-0 mr-4 mt-2">Sign in with</h6>
                    <div class="facebook text-center mr-3">
                        <div class="fa fa-facebook"></div>
                    </div>
                    <div class="twitter text-center mr-3">
                        <div class="fa fa-twitter"></div>
                    </div>
                    <div class="linkedin text-center mr-3">
                        <div class="fa fa-linkedin"></div>
                    </div>
                </div>
                <div class="row px-3 mb-4">
                    <div class="line"></div>
                    <small class="or text-center">Or</small>
                    <div class="line"></div>
                </div>
                <form action="/login/" method="post" modelAttribute="user">
                    <div class="row px-3">
                        <label class="mb-1">
                            <h6 class="mb-0 text-sm">Email Address</h6>
                        </label>
                        <input class="mb-4" type="text" name="email" path="email" value="${email}"
                            placeholder="Enter a valid email address">
                    </div>
                    <div class="row px-3">
                        <label class="mb-1">
                            <h6 class="mb-0 text-sm">Password</h6>
                        </label>
                        <input type="password" name="password" path="password" value="${password}"
                            placeholder="Enter password">
                    </div>
                    <div class="row px-3 mb-4">
                        <div class="custom-control custom-checkbox custom-control-inline">
                            <input id="chk1" type="checkbox" name="check" class="custom-control-input">
                            <label for="chk1" class="custom-control-label text-sm">Remember me</label>
                        </div>
                        <a href="#" class="ml-auto mb-0 text-sm">Forgot Password?</a>
                    </div>
                    <div class="row mb-3 px-3">
                        <button formaction="/login/check/" type="submit" class="btn btn-blue text-center">Login</button>
                    </div>
                    <div class="row mb-4 px-3">
                        <small class="font-weight-bold">Don't have an account? <a href="/register/"
                                class="text-danger ">Register</a></small>
                    </div>
                </form>
            </div>
        </div>
    </body>

    </html>