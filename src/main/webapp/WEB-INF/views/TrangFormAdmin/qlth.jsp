<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <!DOCTYPE html>
        <html>

        <head>
            <meta charset="utf-8">
            <title>Insert title here</title>
            <link rel="stylesheet" href="../css/qlsp.css">
            <link rel="stylesheet" href="../css/admin.css">
            <link href="img/favicon.ico" rel="icon">

            <!-- Google Web Fonts -->
            <link rel="preconnect" href="https://fonts.gstatic.com">
            <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">

            <!-- Font Awesome -->
            <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">

            <!-- Libraries Stylesheet -->
            <link href="../lib/animate/animate.min.css" rel="stylesheet">
            <link href="../lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">

        </head>

        <body>
            <div class="mt-4 p-2">
                <div id="wrapper" class="rounded rounded-5">
                    <h1 class="text-dark">Bảng quản lý thương hiệu</h1>
                    <div class="tablecuon" style="overflow-x: scroll; max-width: 100%;">
                        <table id="keywords" class="table table-bordered table-striped">
                            <thead class="bg-info">
                                <tr>
                                    <th>EDIT</th>
                                    <th><span>Mã Thương Hiệu</span></th>
                                    <th><span>Tên Thương Hiệu</span></th>
                                    <th><span>Logo</span></th>

                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="th" items="${th.content}">
                                    <tr>
                                        <td><a href="/qlth/edit/${th.thid}"
                                                class="text-danger text-decoration-none">EDIT</a></td>
                                        <td>${th.thid}</td>
                                        <td>${th.tenth}</td>
                                        <td><img class="w-50" src="${th.logo}" alt=""></td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                    <div class="text-center my-3">
                        <a class="p-4" href="/qlth/?p=0"><button class="button1 btn-info">First</button></a>
                        <a class="p-4" href="/qlth/?p=${th.number-1}"><button
                                class="button1 btn-info">Previous</button></a>
                        <a class="p-4" href="/qlth/?p=${th.number+1}"><button class="button1 btn-info">Next</button></a>
                        <a class="p-4" href="/qlth/?p=${th.totalPages-1}"><button
                                class="button1 btn-info">Last</button></a>
                    </div>
                </div>
            </div>
            <div class="container-fluid px-1 py-3 mx-auto p-2">
                <div class="row d-flex justify-content-center">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-lg-12 text-center">
                        <h3 class="text-dark">FORM QUẢN LÝ THƯƠNG HIỆU</h3>
                        <p class="blue-text">Xin hãy điền đầy đủ thông tin trong form trước khi dùng các chức năng !!!
                        </p>
                        <div class="card w-100">
                            <h5 class="text-center mb-4">QUẢN LÝ THƯƠNG HIỆU</h5>
                            <form class="form-card" action="/qlth/" method="post" modelAttribute="th">
                                <div class=" row justify-content-between text-left ">
                                    <div class="form-group col-sm-3 flex-column d-flex">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold">Mã Thương Hiệu</span>
                                        </label>
                                        <input class="input" type="text" id="fname" name="thid" path="thid"
                                            value="${th1.thid}" placeholder="Mã Thương Hiệu">
                                    </div>
                                    <div class="form-group col-sm-3 flex-column d-flex">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold">Tên Thương Hiệu</span>
                                        </label>
                                        <input class="input" type="text" id="fname" name="tenth" path="tenth"
                                            value="${th1.tenth}" placeholder="Tên Thương Hiệu">
                                    </div>
                                    <div class="form-group col-sm-3 flex-column d-flex">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold">Logo</span>
                                        </label>
                                        <input class="input" type="text" id="fname" name="logo" path="logo"
                                            value="${th1.logo}" placeholder="Logo">
                                    </div>

                                </div>

                                <div class="row justify-content-end">
                                    <div class="form-group col-sm-3">
                                        <button formaction="/qlth/insert" type="submit"
                                            class="btn-block btn-info button1">Thêm</button>
                                    </div>
                                    <div class="form-group col-sm-3">
                                        <button formaction="/qlth/update" type="submit"
                                            class="btn-block btn-info button1">Sửa</button>
                                    </div>
                                    <div class="form-group col-sm-3">
                                        <button formaction="/qlth/delete/${th1.thid}"
                                            class="btn-block btn-info button1">Xóa</button>
                                    </div>
                                    <div class="form-group col-sm-3">
                                        <button formaction="/qlth/clean" type="submit"
                                            class="btn-block btn-info button1">Làm mới</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </body>

        </html>