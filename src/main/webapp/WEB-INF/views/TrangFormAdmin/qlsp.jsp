<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <!DOCTYPE html>
        <html>

        <head>
            <meta charset="utf-8">
            <title>Insert title here</title>
            <link rel="stylesheet" href="../css/qlsp.css">
            <link rel="stylesheet" href="../css/admin.css">
            <link href="img/favicon.ico" rel="icon">

            <!-- Google Web Fonts -->
            <link rel="preconnect" href="https://fonts.gstatic.com">
            <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">

            <!-- Font Awesome -->
            <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">

            <!-- Libraries Stylesheet -->
            <link href="../lib/animate/animate.min.css" rel="stylesheet">
            <link href="../lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">

        </head>

        <body>
            <div class="mt-4 p-2">
                <div id="wrapper" class="rounded rounded-5">
                    <h1 class="text-dark">Bảng quản lý sản phẩm</h1>
                    <div class="tablecuon" style="overflow-x: scroll; max-width: 100%;">
                        <table id="keywords" class="table table-bordered table-striped">
                            <thead class="bg-info">
                                <tr>
                                    <th>EDIT</th>
                                    <th><span>Tên Sản Phẩm</span></th>
                                    <th><span>Thương Hiệu</span></th>
                                    <th><span>Giá</span></th>
                                    <th><span>Giá Cũ</span></th>
                                    <th><span>Hình</span></th>
                                    <th><span>Màu Sắc</span></th>
                                    <th><span>CPU</span></th>
                                    <th><span>Số Nhân</span></th>
                                    <th><span>Số Luồng</span></th>
                                    <th><span>Tốc Độ CPU</span></th>
                                    <th><span>Tốc Độ Tối Đa</span></th>
                                    <th><span>Ram</span></th>
                                    <th><span>Loại Ram</span></th>
                                    <th><span>Tốc Độ Bus Ram</span></th>
                                    <th><span>Hỗ Trợ Ram Tối Đa</span></th>
                                    <th><span>Rom</span></th>
                                    <th><span>Màn Hình</span></th>
                                    <th><span>Độ Phân Giải</span></th>
                                    <th><span>Tần Số Quét</span></th>
                                    <th><span>Công Nghệ Màn Hình</span></th>
                                    <th><span>Card Màn Hình</span></th>
                                    <th><span>Công Nghệ Âm Thanh</span></th>
                                    <th><span>Tính Năng Khác</span></th>
                                    <th><span>Đèn Bàn Phím</span></th>
                                    <th><span>Hệ Điều Hành</span></th>
                                    <th><span>Thông Tin Pin</span></th>
                                    <th><span>Thiết Kế</span></th>
                                    <th><span>Ngày Ra Mắt</span></th>
                                    <th><span>Kích Thước</span></th>
                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="sp" items="${sp.content}">
                                    <tr>
                                        <td><a href="/qlsp/edit/${sp.spid}"
                                                class="text-danger text-decoration-none">EDIT</a></td>
                                        <td>${sp.tensp}</td>
                                        <td>${sp.thuonghieu.tenth}</td>
                                        <td>${sp.gia}</td>
                                        <td>${sp.giacu}</td>
                                        <td><img style="max-width: 200px;" src="../images/${sp.hinh}" alt=""></td>
                                        <td>${sp.mausac}</td>
                                        <td>${sp.cpu}</td>
                                        <td>${sp.sonhan}</td>
                                        <td>${sp.soluong}</td>
                                        <td>${sp.tocdocpu}</td>
                                        <td>${sp.tocdotoida}</td>
                                        <td>${sp.ram}</td>
                                        <td>${sp.loairam}</td>
                                        <td>${sp.tocdobusram}</td>
                                        <td>${sp.hotroramtoida}</td>
                                        <td>${sp.rom}</td>
                                        <td>${sp.manhinh}</td>
                                        <td>${sp.dophangiai}</td>
                                        <td>${sp.tansoquet}</td>
                                        <td>${sp.congnghemanhinh}</td>
                                        <td>${sp.cardmh}</td>
                                        <td>${sp.congngheamthanh}</td>
                                        <td>${sp.tinhnangkhac}</td>
                                        <td>${sp.denbanphim}</td>
                                        <td>${sp.hdh}</td>
                                        <td>${sp.thongtinpin}</td>
                                        <td>${sp.thietke}</td>
                                        <td>${sp.ngaysx}</td>
                                        <td>${sp.thietke}</td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                    <div class="text-center my-3">
                        <a class="p-4" href="/qlsp/?p=0"><button class="button1 btn-info">First</button></a>
                        <a class="p-4" href="/qlsp/?p=${sp.number-1}"><button
                                class="button1 btn-info">Previous</button></a>
                        <a class="p-4" href="/qlsp/?p=${sp.number+1}"><button class="button1 btn-info">Next</button></a>
                        <a class="p-4" href="/qlsp/?p=${sp.totalPages-1}"><button
                                class="button1 btn-info">Last</button></a>
                    </div>
                </div>
            </div>
            <div class="container-fluid px-1 py-3 mx-auto p-2">
                <div class="row d-flex justify-content-center">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-lg-12 text-center">
                        <h3 class="text-dark">FORM QUẢN LÝ SẢN PHẨM</h3>
                        <p class="blue-text">Xin hãy điền đầy đủ thông tin trong form trước khi dùng các chức năng !!!
                        </p>
                        <div class="card w-100">
                            <h5 class="text-center mb-4">QUẢN LÝ SẢN PHẨM</h5>
                            <form class="form-card" action="/qlsp/" method="post" modelAttribute="sp">
                                <div class=" row justify-content-between text-left ">
                                    <div class="form-group col-sm-3 flex-column d-flex">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold">Mã Sản phẩm</span>
                                        </label>
                                        <input class="input" type="text" id="fname" name="spid" path="spid" disabled
                                            value="${sp1.spid}" placeholder="Mã sản phẩm">
                                    </div>
                                    <div class="form-group col-sm-3 flex-column d-flex">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold">Tên sản phẩm</span>
                                        </label>
                                        <input class="input" type="text" id="fname" name="tensp" path="tensp"
                                            value="${sp1.tensp}" placeholder="Tên sản phẩm">
                                    </div>
                                    <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold danger">Thương hiệu</span>
                                        </label>
                                        <select style="height: 42px;" class="form-control rounded mt-1"
                                            name="thuonghieu" path="thid" id="">
                                            <c:forEach var="th" items="${th}">
                                                <option ${th.thid==sp1.getThuonghieu().getThid()?"selected":""}
                                                    value="${th.thid}">${th.thid}</option>
                                            </c:forEach>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold">Giá</span>
                                        </label>
                                        <input class="input" type="text" id="lname" name="gia" path="gia"
                                            value="${sp1.gia}" placeholder="Giá">
                                    </div>
                                    <div class="form-group col-sm-3 flex-column d-flex">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold">Giá cũ</span>
                                        </label>
                                        <input class="input" type="text" id="fname" name="giacu" path="giacu"
                                            value="${sp1.giacu}" placeholder="Giá cũ">
                                    </div>
                                    <div class="form-group col-sm-3 flex-column d-flex">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold">hình</span>
                                        </label>
                                        <input class="input" type="file" id="file-input" name="hinh" path="hinh"
                                            placeholder="Tên sản phẩm">
                                    </div>
                                    <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold danger">Màu Sắc</span>
                                        </label>
                                        <select style="height: 42px;" class="form-control rounded mt-1" path="mausac"
                                            name="mausac" id="">
                                            <option ${sp1.mausac=="Đen" ?"selected":""} value="Đen">Đen</option>
                                            <option ${sp1.mausac=="Xanh" ?"selected":""} value="Xanh">Xanh</option>
                                            <option ${sp1.mausac=="Đỏ" ?"selected":""} value="Đỏ">Đỏ</option>
                                            <option ${sp1.mausac=="Tím" ?"selected":""} value="Tím">Tím</option>
                                            <option ${sp1.mausac=="Trắng" ?"selected":""} value="Trắng">Trắng</option>
                                            <option ${sp1.mausac=="Bạc" ?"selected":""} value="Bạc">Bạc</option>
                                            <option ${sp1.mausac=="Xanh lam" ?"selected":""} value="Xanh lam">Xanh lam
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold danger">cpu</span>
                                        </label>
                                        <select style="height: 42px;" class="form-control rounded mt-1" path="cpu"
                                            name="cpu" id="">
                                            <option ${sp1.cpu=="Intel Core i5 Comet Lake - 10300H" ?"selected":""}
                                                value="Intel Core i5 Comet Lake - 10300H">Intel Core i5 Comet Lake -
                                                10300H</option>
                                            <option ${sp1.cpu=="AMD Ryzen 5 - 5500U" ?"selected":""}
                                                value="AMD Ryzen 5 - 5500U">AMD Ryzen
                                                5 - 5500U</option>
                                            <option ${sp1.cpu=="Intel Core i5 Tiger Lake - 11320H" ?"selected":""}
                                                value="Intel Core i5 Tiger Lake - 11320H">Intel Core i5 Tiger Lake -
                                                11320H</option>
                                            <option ${sp1.cpu=="Intel Core i5 Tiger Lake - 11400H" ?"selected":""}
                                                value="Intel Core i5 Tiger Lake - 11400H">Intel Core i5 Tiger Lake -
                                                11400H</option>
                                            <option ${sp1.cpu=="Intel Core i5 Alder Lake - 12500H" ?"selected":""}
                                                value="Intel Core i5 Alder Lake - 12500H">Intel Core i5 Alder Lake -
                                                12500H</option>
                                            <option ${sp1.cpu=="Intel Core i3 Tiger Lake - 1115G4" ?"selected":""}
                                                value="Intel Core i3 Tiger Lake - 1115G4">Intel Core i3 Tiger Lake -
                                                1115G4</option>
                                            <option ${sp1.cpu=="Intel Core i7 Tiger Lake - 1165G7" ?"selected":""}
                                                value="Intel Core i7 Tiger Lake - 1165G7">Intel Core i7 Tiger Lake -
                                                1165G7</option>
                                            <option ${sp1.cpu=="Intel Core i7 Tiger Lake - 1165G8" ?"selected":""}
                                                value="Intel Core i7 Tiger Lake - 1165G8">Intel Core i7 Tiger Lake -
                                                1165G8</option>
                                            <option ${sp1.cpu=="Intel Core i7 Tiger Lake - 1165G7" ?"selected":""}
                                                value="Intel Core i5 Tiger Lake - 1135G7">Intel Core i5 Tiger Lake -
                                                1135G7</option>
                                            <option ${sp1.cpu=="Intel Core i5 Tiger Lake - 1135G7" ?"selected":""}
                                                value="Intel Core i5 Raptor Lake - 1340P">Intel Core i5 Raptor Lake -
                                                1340P</option>
                                            <option ${sp1.cpu=="Intel Core i5 Raptor Lake - 1340P" ?"selected":""}
                                                value="Intel Core i7 Tiger Lake - 1165G7">Intel Core i7 Tiger Lake -
                                                1165G7</option>
                                            <option ${sp1.cpu=="Intel Core i7 Alder Lake - 1250U" ?"selected":""}
                                                value="Intel Core i7 Alder Lake - 1250U">Intel Core i7 Alder Lake -
                                                1250U</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold">Số nhân</span>
                                        </label>
                                        <input class="input" type="number" name="sonhan" path="sonhan"
                                            value="${sp1.sonhan}" placeholder="Số nhân">
                                    </div>
                                    <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold">số luồng</span>
                                        </label>
                                        <input class="input" type="number" name="soluong" path="soluong"
                                            value="${sp1.soluong}" placeholder="Số luồng">
                                    </div>
                                    <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold danger">tốc độ cpu</span>
                                        </label>
                                        <select style="height: 42px;" class="form-control rounded mt-1" path="tocdocpu"
                                            name="tocdocpu" id="">
                                            <option ${sp1.tocdocpu=="2.50 GHz" ?"selected":""} value="2.50 GHz">2.50 GHz
                                            </option>
                                            <option ${sp1.tocdocpu=="2.10 GHz" ?"selected":""} value="2.10 GHz">2.10 GHz
                                            </option>
                                            <option ${sp1.tocdocpu=="3.20 GHz" ?"selected":""} value="3.20 GHz">3.20 GHz
                                            </option>
                                            <option ${sp1.tocdocpu=="2.70 GHz" ?"selected":""} value="2.70 GHz">2.70 GHz
                                            </option>
                                            <option ${sp1.tocdocpu=="2.50 GHz" ?"selected":""} value="2.50 GHz">2.50 GHz
                                            </option>
                                            <option ${sp1.tocdocpu=="3.00 GHz" ?"selected":""} value="3.00 GHz">3.00 GHz
                                            </option>
                                            <option ${sp1.tocdocpu=="2.80 GHz" ?"selected":""} value="2.80 GHz">2.80 GHz
                                            </option>
                                            <option ${sp1.tocdocpu=="2.40 GHz" ?"selected":""} value="2.40 GHz">2.40 GHz
                                            </option>
                                            <option ${sp1.tocdocpu=="1.90 GHz" ?"selected":""} value="1.90 GHz">1.90 GHz
                                            </option>
                                            <option ${sp1.tocdocpu=="1.10 GHz" ?"selected":""} value="1.10 GHz">1.10 GHz
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold danger">tốc độ tối đa</span>
                                        </label>
                                        <select style="height: 42px;" class="form-control rounded mt-1"
                                            path="tocdotoida" name="tocdotoida" id="">
                                            <option ${sp1.tocdotoida=="Turbo Boost 4,5 GHz" ?"selected":""}
                                                value="Turbo Boost 4,5 GHz">Turbo Boost 4,5 GHz</option>
                                            <option ${sp1.tocdotoida=="Turbo Boost 4.0 GHz" ?"selected":""}
                                                value="Turbo Boost 4.0 GHz">
                                                Turbo Boost 4.0 GHz</option>
                                            <option ${sp1.tocdotoida=="Turbo Boost 4.1 GHz" ?"selected":""}
                                                value="Turbo Boost 4.1 GHz">Turbo Boost 4.1 GHz
                                            </option>
                                            <option ${sp1.tocdotoida=="Turbo Boost 4.6 GHz" ?"selected":""}
                                                value="Turbo Boost 4.6 GHz">Turbo Boost 4.6 GHz
                                            </option>
                                            <option ${sp1.tocdotoida=="Turbo Boost 4.7 GHz" ?"selected":""}
                                                value="Turbo Boost 4.7 GHz">Turbo Boost 4.7 GHz
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold danger">ram</span>
                                        </label>
                                        <select style="height: 42px;" class="form-control rounded mt-1" path="ram"
                                            name="ram" id="">
                                            <option ${sp1.ram=="8 GB" ?"selected":""} value="8 GB">8 GB
                                            </option>
                                            <option {sp1.ram=="16 GB" ?"selected":""} value="16 GB">16 GB
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold danger">loại ram</span>
                                        </label>
                                        <select style="height: 42px;" class="form-control rounded mt-1" path="loairam"
                                            name="loairam" id="">
                                            <option {sp1.loairam=="DDR4 2 khe (1 khe 8 GB + 1 khe rời)" ?"selected":""}
                                                value="DDR4 2 khe (1 khe 8 GB + 1 khe rời)">DDR4 2 khe (1 khe 8 GB + 1
                                                khe rời)</option>
                                            <option {sp1.loairam=="DDR4 (Onboard)" ?"selected":""}
                                                value="DDR4 (Onboard)">DDR4 (Onboard)
                                            </option>
                                            <option {sp1.loairam=="LPDDR4X (Onboard)" ?"selected":""}
                                                value="LPDDR4X (Onboard)">LPDDR4X (Onboard)</option>
                                            <option {sp1.loairam=="LPDDR5 (Onboard)" ?"selected":""}
                                                value="LPDDR5 (Onboard)">LPDDR5 (Onboard)</option>
                                            <option {sp1.loairam=="LPDDR4 (Onboard)" ?"selected":""}
                                                value="LPDDR4 (Onboard)">LLPDDR4 (Onboard)</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold danger">tốc độ bus ram</span>
                                        </label>
                                        <select style="height: 42px;" class="form-control rounded mt-1"
                                            path="tocdobusram" name="tocdobusram" id="">
                                            <option {sp1.tocdobusram=="2933 MHz" ?"selected":""} value="2933 MHz">2933
                                                MHz
                                            </option>
                                            <option {sp1.tocdobusram=="3200 MHz" ?"selected":""} value="3200 MHz">3200
                                                MHz</option>
                                            <option {sp1.tocdobusram=="4267 MHz" ?"selected":""} value="4267 MHz">4267
                                                MHz</option>
                                            <option {sp1.tocdobusram=="4266 MHz" ?"selected":""} value="4266 MHz">4266
                                                MHz</option>
                                            <option {sp1.tocdobusram=="6000 MHz" ?"selected":""} value="6000 MHz">6000
                                                MHz</option>
                                            <option {sp1.tocdobusram=="4267 MHz" ?"selected":""} value="4267 MHz">4267
                                                MHz</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold danger">hỗ trợ ram tối đa</span>
                                        </label>
                                        <select style="height: 42px;" class="form-control rounded mt-1"
                                            path="hotroramtoida" name="hotroramtoida" id="">
                                            <option {sp1.hotroramtoida=="16 GB" ?"selected":""} value="16 GB">16 GB
                                            </option>
                                            <option {sp1.hotroramtoida=="32 GB" ?"selected":""} value="32 GB">32 GB
                                            </option>
                                            <option {sp1.hotroramtoida=="32 GB" ?"selected":""} value="32 GB">32 GB
                                            </option>
                                            <option {sp1.hotroramtoida=="Không hỗ trợ nâng cấp" ?"selected":""}
                                                value="Không hỗ trợ nâng cấp">Không hỗ trợ nâng cấp</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold danger">rom</span>
                                        </label>
                                        <select style="height: 42px;" class="form-control rounded mt-1" path="rom"
                                            name="rom" id="">
                                            <option
                                                {sp1.rom=="512 GB SSD NVMe PCIe (Có thể tháo ra, lắp thanh khác tối đa 1 TB)"
                                                ?"selected":""}
                                                value="512 GB SSD NVMe PCIe (Có thể tháo ra, lắp thanh khác tối đa 1 TB)">
                                                512 GB SSD NVMe PCIe (Có thể tháo ra, lắp thanh
                                                khác tối đa 1 TB)</option>
                                            <option
                                                {sp1.rom=="Hỗ trợ khe cắm HDD SATA 2.5 inch mở rộng (nâng cấp tối đa 1 TB)512 GB SSD NVMe PCIe (Có thể tháo ra, lắp thanh khác tối đa 1 TB (2280) / 512 GB (2242))"
                                                ?"selected":""}
                                                value="Hỗ trợ khe cắm HDD SATA 2.5 inch mở rộng (nâng cấp tối đa 1 TB)512 GB SSD NVMe PCIe (Có thể tháo ra, lắp thanh khác tối đa 1 TB (2280) / 512 GB (2242))">
                                                Hỗ trợ khe cắm HDD SATA 2.5 inch mở rộng (nâng
                                                cấp tối đa 1 TB)512 GB SSD NVMe PCIe (Có thể tháo ra, lắp thanh khác tối
                                                đa 1 TB (2280) / 512 GB (2242))</option>
                                            <option
                                                {sp1.rom=="Hỗ trợ khe cắm SATA 2.5 inch mở rộng (nâng cấp SSD hoặc HDD đều được)512 GB SSD NVMe PCIe"
                                                ?"selected":""}
                                                value="Hỗ trợ khe cắm SATA 2.5 inch mở rộng (nâng cấp SSD hoặc HDD đều được)512 GB SSD NVMe PCIe">
                                                Hỗ trợ khe cắm SATA 2.5 inch mở rộng (nâng cấp SSD hoặc HDD đều được)512
                                                GB SSD NVMe PCIe</option>
                                            <option
                                                {sp1.rom=="512 GB SSD NVMe PCIe (Có thể tháo ra, lắp thanh khác tối đa 2 TB)"
                                                ?"selected":""}
                                                value="512 GB SSD NVMe PCIe (Có thể tháo ra, lắp thanh khác tối đa 2 TB)">
                                                512 GB SSD NVMe PCIe (Có thể tháo ra, lắp thanh khác tối đa 2 TB)
                                            </option>
                                            <option
                                                {sp1.rom=="512 GB SSD NVMe PCIe (Có thể tháo ra, lắp thanh khác tối đa 1 TB)"
                                                ?"selected":""}
                                                value="512 GB SSD NVMe PCIe (Có thể tháo ra, lắp thanh khác tối đa 1 TB)">
                                                512 GB SSD NVMe PCIe (Có thể tháo ra, lắp thanh khác tối đa 1 TB)
                                            </option>
                                            <option
                                                {sp1.rom=="256 GB SSD NVMe PCIeHỗ trợ khe cắm HDD SATA 2.5 inch mở rộng"
                                                ?"selected":""}
                                                value="256 GB SSD NVMe PCIeHỗ trợ khe cắm HDD SATA 2.5 inch mở rộng">256
                                                GB SSD NVMe PCIeHỗ trợ khe cắm HDD SATA 2.5 inch mở rộng</option>
                                            <option {sp1.rom=="1 TB SSD" ?"selected":""} value="1 TB SSD">1 TB SSD
                                            </option>
                                            <option
                                                {sp1.rom=="512 GB SSD NVMe PCIe Gen 4.0Hỗ trợ thêm 1 khe cắm SSD M.2 PCIe Gen 4 mở rộng"
                                                ?"selected":""}
                                                value="512 GB SSD NVMe PCIe Gen 4.0Hỗ trợ thêm 1 khe cắm SSD M.2 PCIe Gen 4 mở rộng">
                                                512 GB SSD NVMe PCIe Gen 4.0Hỗ trợ thêm 1 khe cắm SSD M.2 PCIe Gen 4 mở
                                                rộng</option>
                                            <option {sp1.rom=="512 GB SSD NVMe PCIe" ?"selected":""}
                                                value="512 GB SSD NVMe PCIe">
                                                512 GB SSD NVMe PCIe</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold danger">màn hình</span>
                                        </label>
                                        <select style="height: 42px;" class="form-control rounded mt-1" path="manhinh"
                                            name="manhinh" id="">
                                            <option {sp1.manhinh=="15.6 inch" ?"selected":""} value="15.6 inch">15.6
                                                inch</option>
                                            <option {sp1.manhinh=="13 inch" ?"selected":""} value="13 inch">13 inch
                                            </option>
                                            <option {sp1.manhinh=="13.3 inch" ?"selected":""} value="13.3 inch">13.3
                                                inch</option>
                                            <option {sp1.manhinh=="14 inch" ?"selected":""} value="14 inch">14 inch
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold danger">độ phân giải</span>
                                        </label>
                                        <select style="height: 42px;" class="form-control rounded mt-1"
                                            path="dophangiai" name="dophangiai" id="">
                                            <option {sp1.manhinh=="Full HD (1920 x 1080)" ?"selected":""}
                                                value="Full HD (1920 x 1080)">Full HD (1920 x 1080)</option>
                                            <option {sp1.manhinh=="WQHD (2160x1350)" ?"selected":""}
                                                value="WQHD (2160x1350)">WQHD (2160x1350)
                                            </option>
                                            <option {sp1.manhinh=="2.8K (2880 x 1800) - OLED 16:10" ?"selected":""}
                                                value="2.8K (2880 x 1800) - OLED 16:10">2.8K (2880 x 1800) - OLED 16:10
                                            </option>
                                            <option {sp1.manhinh=="2.8K (2880 x 1800) - OLED" ?"selected":""}
                                                value="2.8K (2880 x 1800) - OLED">2.8K (2880 x 1800) - OLED
                                            </option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold danger">tần số quét</span>
                                        </label>
                                        <select style="height: 42px;" class="form-control rounded mt-1" path="tansoquet"
                                            name="tansoquet" id="">
                                            <option {sp1.tansoquet=="144 Hz" ?"selected":""} value="144 Hz">144 Hz
                                            </option>
                                            <option {sp1.tansoquet=="120 Hz" ?"selected":""} value="120 Hz">120 Hz
                                            </option>
                                            <option {sp1.tansoquet=="60 Hz" ?"selected":""} value="60 Hz">60 Hz</option>
                                            <option {sp1.tansoquet=="90 Hz" ?"selected":""} value="90 Hz">90 Hz</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold danger">công nghệ màn hình</span>
                                        </label>
                                        <select style="height: 42px;" class="form-control rounded mt-1"
                                            path="congnghemanhinh" name="congnghemanhinh" id="">
                                            <option {sp1.congnghemanhinh=="Tấm nền IPS, Chống chói Anti Glare, 250 nits"
                                                ?"selected":""} value="Tấm nền IPS, Chống chói Anti Glare, 250 nits">Tấm
                                                nền IPS, Chống chói Anti Glare, 250 nits</option>
                                            <option {sp1.congnghemanhinh=="LED Backlit, Công nghệ IPS, Acer ComfyView"
                                                ?"selected":""} value="LED Backlit, Công nghệ IPS, Acer ComfyView">LED
                                                Backlit, Công nghệ IPS, Acer ComfyView</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold danger">card màn hình</span>
                                        </label>
                                        <select style="height: 42px;" class="form-control rounded mt-1" path="cardmh"
                                            name="cardmh" id="">
                                            <option {sp1.cardmh=="Card rời - NVIDIA GeForce GTX 1650 4 GB"
                                                ?"selected":""} value="Card rời - NVIDIA GeForce GTX 1650 4 GB">Card rời
                                                - NVIDIA GeForce GTX 1650 4 GB</option>
                                            <option {sp1.cardmh=="Card tích hợp - Intel UHD Graphics" ?"selected":""}
                                                value="Card tích hợp - Intel UHD Graphics">Card tích hợp - Intel UHD
                                                Graphics</option>
                                            <option {sp1.cardmh=="Card tích hợp - Intel Iris Xe Graphics"
                                                ?"selected":""} value="Card tích hợp - Intel Iris Xe Graphics">Card tích
                                                hợp - Intel Iris Xe Graphics</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold danger">công nghệ âm thanh</span>
                                        </label>
                                        <select style="height: 42px;" class="form-control rounded mt-1"
                                            path="congngheamthanh" name="congngheamthanh" id="">
                                            <option {sp1.congngheamthanh=="DTS X:Ultra Audio" ?"selected":""}
                                                value="DTS X:Ultra Audio">DTS X:Ultra Audio</option>
                                            <option {sp1.congngheamthanh=="Acer TrueHarmony" ?"selected":""}
                                                value="Acer TrueHarmony">Acer TrueHarmony</option>
                                            <option {sp1.congngheamthanh=="Nahimic Audio" ?"selected":""}
                                                value="Nahimic Audio">
                                                Nahimic Audio</option>
                                            <option {sp1.congngheamthanh=="Realtek High Definition Audio"
                                                ?"selected":""} value="Realtek High Definition Audio">
                                                Realtek High Definition Audio</option>
                                            <option {sp1.congngheamthanh=="Audio by B&OHP Audio Boost" ?"selected":""}
                                                value="Audio by B&OHP Audio Boost">
                                                Audio by B&OHP Audio Boost</option>
                                            <option {sp1.congngheamthanh=="SonicMaster audio" ?"selected":""}
                                                value="SonicMaster audio">
                                                SonicMaster audio</option>
                                            <option {sp1.congngheamthanh=="Công nghệ Smart AMPDolby Audio"
                                                ?"selected":""} value="Công nghệ Smart AMPDolby Audio">
                                                Công nghệ Smart AMPDolby Audio</option>
                                            <option {sp1.congngheamthanh=="Audio by Harman/Kardon" ?"selected":""}
                                                value="Audio by Harman/Kardon">
                                                Audio by Harman/Kardon</option>
                                            <option {sp1.congngheamthanh=="Acer TrueHarmonyDTS Audio" ?"selected":""}
                                                value="Acer TrueHarmonyDTS Audio">
                                                Acer TrueHarmonyDTS Audio</option>
                                            <option {sp1.congngheamthanh=="High Definition (HD) AudioDolby Atmos"
                                                ?"selected":""} value="High Definition (HD) AudioDolby Atmos">
                                                High Definition (HD) AudioDolby Atmos</option>
                                            <option {sp1.congngheamthanh=="HP Audio BoostBang & Olufsen audio"
                                                ?"selected":""} value="HP Audio BoostBang & Olufsen audio">
                                                HP Audio BoostBang & Olufsen audio</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold danger">tính năng khác</span>
                                        </label>
                                        <select style="height: 42px;" class="form-control rounded mt-1"
                                            path="tinhnangkhac" name="tinhnangkhac" id="">
                                            <option {sp1.tinhnangkhac=="Đèn bàn phím chuyển màu RGB" ?"selected":""}
                                                value="Đèn bàn phím chuyển màu RGB">Đèn bàn phím chuyển màu RGB</option>
                                            <option {sp1.tinhnangkhac=="Bản lề mở 180 độ" ?"selected":""}
                                                value="Bản lề mở 180 độ">Bản lề mở 180 độ</option>
                                            <option
                                                {sp1.tinhnangkhac=="TPM 2.0, Đèn bàn phím chuyển màu RGB, Công tắc khóa camera"
                                                ?"selected":""}
                                                value="TPM 2.0, Đèn bàn phím chuyển màu RGB, Công tắc khóa camera">TPM
                                                2.0, Đèn bàn phím chuyển màu RGB, Công tắc khóa camera</option>
                                            <option {sp1.tinhnangkhac=="TPM 2.0Công tắc khóa camera" ?"selected":""}
                                                value="TPM 2.0Công tắc khóa camera">TPM 2.0Công tắc khóa camera</option>
                                            <option {sp1.tinhnangkhac=="Bảo mật vân tay" ?"selected":""}
                                                value="Bảo mật vân tay">Bảo mật vân tay</option>
                                            <option {sp1.tinhnangkhac=="TPM 2.0Mở khóa khuôn mặt" ?"selected":""}
                                                value="TPM 2.0Mở khóa khuôn mặt">TPM 2.0Mở khóa khuôn mặt</option>
                                            <option
                                                {sp1.tinhnangkhac=="Tiêu chuẩn Nền Intel Evo, Độ bền chuẩn quân đội MIL STD 810G, Mở khóa khuôn mặt"
                                                ?"selected":""}
                                                value="Tiêu chuẩn Nền Intel Evo, Độ bền chuẩn quân đội MIL STD 810G, Mở khóa khuôn mặt">
                                                Tiêu chuẩn Nền Intel Evo, Độ bền chuẩn quân đội MIL STD 810G, Mở khóa
                                                khuôn mặt</option>
                                            <option {sp1.tinhnangkhac=="Tiêu chuẩn Nền Intel EvoBảo mật vân tay"
                                                ?"selected":""} value="Tiêu chuẩn Nền Intel EvoBảo mật vân tay">Tiêu
                                                chuẩn Nền Intel EvoBảo mật vân tay</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold danger">đèn bàn phím</span>
                                        </label>
                                        <select style="height: 42px;" class="form-control rounded mt-1"
                                            path="denbanphim" name="denbanphim" id="">
                                            <option {sp1.denbanphim=="Có" ?"selected":""} value="Có">Có</option>
                                            <option {sp1.denbanphim=="Không" ?"selected":""} value="Không">Không
                                            </option>
                                            <option {sp1.denbanphim=="Đơn sắc trắng" ?"selected":""}
                                                value="Đơn sắc trắng">Đơn sắc trắng</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold danger">hệ điều hành</span>
                                        </label>
                                        <select style="height: 42px;" class="form-control rounded mt-1" path="hdh"
                                            name="hdh" id="">
                                            <option {sp1.hdh=="Windows 11 Home SL" ?"selected":""}
                                                value="Windows 11 Home SL">Windows 11 Home SL</option>

                                        </select>
                                    </div>
                                    <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold danger">thông tin pin</span>
                                        </label>
                                        <select style="height: 42px;" class="form-control rounded mt-1"
                                            path="thongtinpin" name="thongtinpin" id="">
                                            <option {sp1.thongtinpin=="3-cell Li-ion, 48 Wh" ?"selected":""}
                                                value="3-cell Li-ion, 48 Wh">3-cell Li-ion, 48 Wh
                                            </option>
                                            <option {sp1.thongtinpin=="45 Wh" ?"selected":""} value="45 Wh">45 Wh
                                            </option>
                                            <option {sp1.thongtinpin=="3-cell Li-ion, 51 Wh" ?"selected":""}
                                                value="3-cell Li-ion, 51 Wh">3-cell Li-ion, 51 Wh
                                            </option>
                                            <option {sp1.thongtinpin=="60 Wh" ?"selected":""} value="60 Wh">
                                                60 Wh</option>
                                            <option {sp1.thongtinpin=="4-cell Li-ion, 70 Wh" ?"selected":""}
                                                value="4-cell Li-ion, 70 Wh">
                                                4-cell Li-ion, 70 Wh</option>
                                            <option {sp1.thongtinpin=="2-cell Li-ion, 37 Wh" ?"selected":""}
                                                value="2-cell Li-ion, 37 Wh">
                                                2-cell Li-ion, 37 Wh</option>
                                            <option {sp1.thongtinpin=="41 Wh" ?"selected":""} value="41 Wh">
                                                41 Wh</option>
                                            <option {sp1.thongtinpin=="4-cell Li-ion, 67 Wh" ?"selected":""}
                                                value="4-cell Li-ion, 67 Wh">
                                                4-cell Li-ion, 67 Wh</option>
                                            <option {sp1.thongtinpin=="Li-ion, 56 Wh" ?"selected":""}
                                                value="Li-ion, 56 Wh">
                                                Li-ion, 56 Wh</option>
                                            <option {sp1.thongtinpin=="Li-ion, 72 Wh" ?"selected":""}
                                                value="Li-ion, 72 Wh">
                                                Li-ion, 72 Wh</option>
                                            <option {sp1.thongtinpin=="4-cell Li-ion, 66 Wh" ?"selected":""}
                                                value="4-cell Li-ion, 66 Wh">
                                                4-cell Li-ion, 66 Wh</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold danger">thiết kế</span>
                                        </label>
                                        <select style="height: 42px;" class="form-control rounded mt-1" path="thietke"
                                            name="thietke" id="">
                                            <option {sp1.thietke=="Vỏ nhựa - nắp lưng bằng kim loại" ?"selected":""}
                                                value="Vỏ nhựa - nắp lưng bằng kim loại">Vỏ nhựa - nắp lưng bằng kim
                                                loại</option>
                                            <option {sp1.thietke=="Vỏ nhựa" ?"selected":""} value="Vỏ nhựa">Vỏ nhựa
                                            </option>
                                            <option {sp1.thietke=="Mặt trên Magie Nhôm - Mặt dưới vải nhựa"
                                                ?"selected":""} value="Mặt trên Magie Nhôm - Mặt dưới vải nhựa">Mặt trên
                                                Magie Nhôm - Mặt dưới vải nhựa</option>
                                            <option {sp1.thietke=="Vỏ kim loại nguyên khối" ?"selected":""}
                                                value="Vỏ kim loại nguyên khối">Vỏ kim loại nguyên khối</option>
                                            <option {sp1.thietke=="Vỏ kim loại" ?"selected":""} value="Vỏ kim loại">Vỏ
                                                kim loại</option>
                                            <option
                                                {sp1.thietke=="Vỏ kim loại - Nắp lưng và bàn phím bằng Kính cùng Magie"
                                                ?"selected":""}
                                                value="Vỏ kim loại - Nắp lưng và bàn phím bằng Kính cùng Magie">Vỏ kim
                                                loại - Nắp lưng và bàn phím bằng Kính cùng Magie</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold danger">ngày ra bán</span>
                                        </label>
                                        <select style="height: 42px;" class="form-control rounded mt-1" path="ngaysx"
                                            name="ngaysx" id="">
                                            <option {sp1.ngaysx=="2021" ?"selected":""} value="2021">2021</option>
                                            <option {sp1.ngaysx=="2022" ?"selected":""} value="2022">2022
                                            </option>
                                            <option {sp1.ngaysx=="2023" ?"selected":""} value="2023">2023</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold danger">kích thước</span>
                                        </label>
                                        <select style="height: 42px;" class="form-control rounded mt-1" path="kichthuoc"
                                            name="kichthuoc" id="">
                                            <option
                                                {sp1.kichthuoc=="Dài 359 mm - Rộng 256 mm - Dày 24.9 mm - Nặng 2.3 kg"
                                                ?"selected":""}
                                                value="Dài 359 mm - Rộng 256 mm - Dày 24.9 mm - Nặng 2.3 kg">Dài 359 mm
                                                - Rộng 256 mm - Dày 24.9 mm - Nặng 2.3 kg
                                            </option>
                                            <option
                                                {sp1.kichthuoc=="Dài 363.4 mm - Rộng 254.5 mm - Dày 22.9 mm - Nặng 2.1 kg"
                                                ?"selected":""}
                                                value="Dài 363.4 mm - Rộng 254.5 mm - Dày 22.9 mm - Nặng 2.1 kg">Dài
                                                363.4 mm - Rộng 254.5 mm - Dày 22.9 mm - Nặng 2.1 kg</option>
                                            <option
                                                {sp1.kichthuoc=="Dài 359.6 mm - Rộng 251.9 mm - Dày 24.2 mm - Nặng 2.25 kg"
                                                ?"selected":""}
                                                value="Dài 359.6 mm - Rộng 251.9 mm - Dày 24.2 mm - Nặng 2.25 kg">Dài
                                                359.6 mm - Rộng 251.9 mm - Dày 24.2 mm - Nặng 2.25 kg</option>
                                            <option
                                                {sp1.kichthuoc=="Dài 359 mm - Rộng 254 mm - Dày 21.7 mm - Nặng 1.86 kg"
                                                ?"selected":""}
                                                value="Dài 359 mm - Rộng 254 mm - Dày 21.7 mm - Nặng 1.86 kg">Dài 359 mm
                                                - Rộng 254 mm - Dày 21.7 mm - Nặng 1.86 kg</option>
                                            <option
                                                {sp1.kichthuoc=="Dài 357.9 mm - Rộng 255 mm - Dày 23.5 mm - Nặng 2.29 kg"
                                                ?"selected":""}
                                                value="Dài 357.9 mm - Rộng 255 mm - Dày 23.5 mm - Nặng 2.29 kg">Dài
                                                357.9 mm - Rộng 255 mm - Dày 23.5 mm - Nặng 2.29 kg</option>
                                            <option
                                                {sp1.kichthuoc=="Dài 359.6 mm - Rộng 266.4 mm - Dày 21.8 mm - Nặng 2.315 kg"
                                                ?"selected":""}
                                                value="Dài 359.6 mm - Rộng 266.4 mm - Dày 21.8 mm - Nặng 2.315 kg">Dài
                                                359.6 mm - Rộng 266.4 mm - Dày 21.8 mm - Nặng 2.315 kg</option>
                                            <option
                                                {sp1.kichthuoc=="Dài 325.4 mm - Rộng 216 mm - Dày 19.9 mm - Nặng 1.55 kg"
                                                ?"selected":""}
                                                value="Dài 325.4 mm - Rộng 216 mm - Dày 19.9 mm - Nặng 1.55 kg">Dài
                                                325.4 mm - Rộng 216 mm - Dày 19.9 mm - Nặng 1.55 kg</option>
                                            <option
                                                {sp1.kichthuoc=="Dài 297.4 mm - Rộng 207.4 mm - Dày 9.19 mm - Nặng 1.168 kg"
                                                ?"selected":""}
                                                value="Dài 297.4 mm - Rộng 207.4 mm - Dày 9.19 mm - Nặng 1.168 kg">Dài
                                                297.4 mm - Rộng 207.4 mm - Dày 9.19 mm - Nặng 1.168 kg</option>
                                            <option
                                                {sp1.kichthuoc=="Dài 304.2 mm - Rộng 203 mm - Dày 13.9 mm - Nặng 1.14 kg"
                                                ?"selected":""}
                                                value="Dài 304.2 mm - Rộng 203 mm - Dày 13.9 mm - Nặng 1.14 kg">Dài
                                                304.2 mm - Rộng 203 mm - Dày 13.9 mm - Nặng 1.14 kg</option>
                                            <option
                                                {sp1.kichthuoc=="Dài 322.8 mm - Rộng 212.2 mm - Dày 15.9 mm - Nặng 1.19 kg"
                                                ?"selected":""}
                                                value="Dài 322.8 mm - Rộng 212.2 mm - Dày 15.9 mm - Nặng 1.19 kg">Dài
                                                322.8 mm - Rộng 212.2 mm - Dày 15.9 mm - Nặng 1.19 kg</option>
                                            <option
                                                {sp1.kichthuoc=="Dài 311.6 mm - Rộng 213.9 mm - Dày 15.9 mm - Nặng 0.999 kg"
                                                ?"selected":""}
                                                value="Dài 311.6 mm - Rộng 213.9 mm - Dày 15.9 mm - Nặng 0.999 kg">Dài
                                                311.6 mm - Rộng 213.9 mm - Dày 15.9 mm - Nặng 0.999 kg</option>
                                            <option
                                                {sp1.kichthuoc=="Dài 298.3 mm - Rộng 214.9 mm - Dày 16.1 mm - Nặng 1.34 kg"
                                                ?"selected":""}
                                                value="Dài 298.3 mm - Rộng 214.9 mm - Dày 16.1 mm - Nặng 1.34 kg">Dài
                                                298.3 mm - Rộng 214.9 mm - Dày 16.1 mm - Nặng 1.34 kg</option>
                                        </select>
                                    </div>
                                    <div class="form-group col-sm-6 flex-column d-flex text-dark">
                                        <img id="preview-image" src="#" alt="Ảnh xem trước" style="display: none;">
                                    </div>
                                </div>

                                <div class="row justify-content-end">
                                    <div class="form-group col-sm-3">
                                        <button formaction="/qlsp/insert" type="submit"
                                            class="btn-block btn-info button1">Thêm</button>
                                    </div>
                                    <div class="form-group col-sm-3">
                                        <button formaction="/qlsp/update" type="submit"
                                            class="btn-block btn-info button1">Sửa</button>
                                    </div>
                                    <div class="form-group col-sm-3">
                                        <button formaction="/qlsp/delete/${sp1.spid}" type="submit"
                                            class="btn-block btn-info button1">Xóa</button>
                                    </div>
                                    <div class="form-group col-sm-3">
                                        <button type="submit" class="btn-block btn-info button1">Làm mới</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>

            <script>
                var fileInput = document.getElementById('file-input');
                var previewImage = document.getElementById('preview-image');

                fileInput.addEventListener('change', function (e) {
                    var file = e.target.files[0];
                    var reader = new FileReader();

                    reader.onload = function (e) {
                        previewImage.src = e.target.result;
                        previewImage.style.display = 'block';
                    };

                    reader.readAsDataURL(file);
                });
            </script>
        </body>

        </html>