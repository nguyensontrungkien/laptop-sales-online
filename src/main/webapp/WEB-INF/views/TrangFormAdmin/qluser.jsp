<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <!DOCTYPE html>
        <html>

        <head>
            <meta charset="utf-8">
            <title>Insert title here</title>
            <link rel="stylesheet" href="../css/qlsp.css">
            <link rel="stylesheet" href="../css/admin.css">
            <link href="img/favicon.ico" rel="icon">

            <!-- Google Web Fonts -->
            <link rel="preconnect" href="https://fonts.gstatic.com">
            <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">

            <!-- Font Awesome -->
            <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">

            <!-- Libraries Stylesheet -->
            <link href="../lib/animate/animate.min.css" rel="stylesheet">
            <link href="../lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">

        </head>

        <body>
            <div class="mt-4 p-2">
                <div id="wrapper" class="rounded rounded-5">
                    <h1 class="text-dark">Bảng quản lý user</h1>
                    <div class="tablecuon" style="overflow-x: scroll; max-width: 100%;">
                        <table id="keywords" class="table table-bordered table-striped">
                            <thead class="bg-info">
                                <tr>
                                    <th>EDIT</th>
                                    <th><span>Mã Thương Hiệu</span></th>
                                    <th><span>Tên Thương Hiệu</span></th>
                                    <th><span>Logo</span></th>

                                </tr>
                            </thead>
                            <tbody>
                                <c:forEach var="user" items="${user.content}">
                                    <tr>
                                        <td><a href="/qluser/edit/${user.userid}"
                                                class="text-danger text-decoration-none">EDIT</a></td>
                                        <td>${user.username}</td>
                                        <td>${user.password}</td>
                                        <td>${user.sdt}</td>
                                        <td>${user.email}</td>
                                        <td>${user.diachi}</td>
                                        <td>${user.ngaytao}</td>
                                        <td><img src="../images/${user.image}" alt=""></td>
                                    </tr>
                                </c:forEach>
                            </tbody>
                        </table>
                    </div>
                    <div class="text-center my-3">
                        <a class="p-4" href="/qluser/?p=0"><button class="button1 btn-info">First</button></a>
                        <a class="p-4" href="/qluser/?p=${th.number-1}"><button
                                class="button1 btn-info">Previous</button></a>
                        <a class="p-4" href="/qluser/?p=${th.number+1}"><button
                                class="button1 btn-info">Next</button></a>
                        <a class="p-4" href="/qluser/?p=${th.totalPages-1}"><button
                                class="button1 btn-info">Last</button></a>
                    </div>
                </div>
            </div>
            <div class="container-fluid px-1 py-3 mx-auto p-2">
                <div class="row d-flex justify-content-center">
                    <div class="col-xl-12 col-lg-12 col-md-12 col-lg-12 text-center">
                        <h3 class="text-dark">FORM QUẢN LÝ THƯƠNG HIỆU</h3>
                        <p class="blue-text">Xin hãy điền đầy đủ thông tin trong form trước khi dùng các chức năng !!!
                        </p>
                        <div class="card w-100">
                            <h5 class="text-center mb-4">QUẢN LÝ USER</h5>
                            <form class="form-card" action="/qluser/" method="post" modelAttribute="user">
                                <div class=" row justify-content-between text-left ">
                                    <div class="form-group col-sm-3 flex-column d-flex">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold">Họ và tên</span>
                                        </label>
                                        <input class="input" type="text" id="fname" name="userid" path="userid"
                                            value="${user1.userid}" placeholder="Họ và tên">
                                    </div>
                                    <div class="form-group col-sm-3 flex-column d-flex">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold">Họ và tên</span>
                                        </label>
                                        <input class="input" type="text" id="fname" name="username" path="username"
                                            value="${user1.username}" placeholder="Họ và tên">
                                    </div>
                                    <div class="form-group col-sm-3 flex-column d-flex">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold">Mật khẩu</span>
                                        </label>
                                        <input class="input" type="password" id="fname" name="password" path="password"
                                            value="${user1.password}" placeholder="password">
                                    </div>
                                    <div class="form-group col-sm-3 flex-column d-flex">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold">Email</span>
                                        </label>
                                        <input class="input" type="text" id="fname" name="email" path="email"
                                            value="${user1.email}" placeholder="Email">
                                    </div>
                                    <div class="form-group col-sm-3 flex-column d-flex">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold">Địa chỉ</span>
                                        </label>
                                        <input class="input" type="text" id="fname" name="diachi" path="diachi"
                                            value="${user1.diachi}" placeholder="địa chỉ">
                                    </div>
                                    <div class="form-group col-sm-3 flex-column d-flex">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold">Số điện thoại</span>
                                        </label>
                                        <input class="input" type="text" id="fname" name="sdt" path="sdt"
                                            value="${user1.sdt}" placeholder="Số điện thoại">
                                    </div>
                                    <div class="form-group col-sm-3 flex-column d-flex">
                                        <label class="form-control-label px-3">
                                            <span class="text-info font-weight-bold">Ngày tạo</span>
                                        </label>
                                        <input class="input" type="text" id="fname" name="" path=""
                                            value="${user1.ngaytao}" placeholder="ngày tạo">
                                    </div>

                                </div>

                                <div class="row justify-content-end">
                                    <div class="form-group col-sm-3">
                                        <button formaction="/qluser/insert" type="submit"
                                            class="btn-block btn-info button1">Thêm</button>
                                    </div>
                                    <div class="form-group col-sm-3">
                                        <button formaction="/qluser/update" type="submit"
                                            class="btn-block btn-info button1">Sửa</button>
                                    </div>
                                    <div class="form-group col-sm-3">
                                        <button formaction="/qluser/delete/${th1.thid}"
                                            class="btn-block btn-info button1">Xóa</button>
                                    </div>
                                    <div class="form-group col-sm-3">
                                        <button formaction="/qluser/clean" type="submit"
                                            class="btn-block btn-info button1">Làm mới</button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </body>

        </html>