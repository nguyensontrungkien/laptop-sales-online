<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
  <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <!DOCTYPE html>
    <html>

    <head>
      <meta charset="utf-8">
      <title>Insert title here</title>
      <link rel="stylesheet" href="../css/profile.css">
      <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet" />

    </head>

    <body>
      <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.3.0/css/font-awesome.min.css" rel="stylesheet">
      <div class="container bootstrap snippets bootdey">
        <div class="row">
          <!-- BEGIN USER PROFILE -->
          <div class="col-md-12">
            <div class="grid profile">
              <div class="grid-header row" style="margin:0px;">
                <div class="col-2">
                  <img src="../images/${user.image}" class="rounded-circle" alt="">
                </div>
                <div class="col-7">
                  <h3>${user.username}</h3>
                  <p>${user.email}</p>
                  <form action="" method="post" enctype="multipart/form-data">
                    <input type="file" class="my-1" name="attach"><br>
                    <button class="btn btn-dark rounded" type="submit" formaction="/profile/updateimage/">
                      update
                    </button>
                  </form>

                </div>
                <div class="col-xs-3 text-right">
                  <p><a href="" title="Everyone can see your profile"><i class="fa fa-globe"></i> Everyone</a></p>
                </div>
              </div>
              <div class="grid-body">
                <ul class="nav nav-tabs" id="myTab" role="tablist">
                  <li class="nav-item" role="presentation">
                    <a class="nav-link active" id="home-tab" data-toggle="tab" href="#home" role=" tab"
                      aria-controls="#home" aria-selected="true">Home</a>
                  </li>
                  <li class="nav-item" role="presentation">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#profile" role="tab"
                      aria-controls="#profile" aria-selected="false">Profile</a>
                  </li>
                  <li class="nav-item" role="presentation">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#update" role="tab"
                      aria-controls="#update" aria-selected="false">Cập nhật thông tin</a>
                  </li>
                  <li class="nav-item" role="presentation">
                    <a class="nav-link" id="profile-tab" data-toggle="tab" href="#mk" role="tab" aria-controls="#mk"
                      aria-selected="false">Cập nhật mật khẩu</a>
                  </li>
                </ul>
                <div class="tab-content">
                  <!-- BEGIN PROFILE -->
                  <div class="tab-pane" id="profile">
                    <p class="lead">My Profile</p>
                    <hr>
                    <div class="row">
                      <div class="col-md-6">
                        <p><strong>Họ và tên: </strong>${user.username}</p>
                        <p><strong>Email: </strong> <a href="mailto:jwilliams@gmail.com">${user.email}</a></p>
                        <p><strong>Địa chỉ: </strong>${user.diachi}</p>
                        <p><strong>Ngày đăng kí :</strong>${user.ngaytao}</p>
                        <p><strong>Hobbies:</strong> Read books, hang out, learn history, making website</p>
                      </div>
                      <div class="col-md-6">
                        <p><strong>Số điện thoại: </strong>${user.sdt}</p>
                        <p><strong>Chức vụ: </strong> Người dùng</p>
                        <p><strong>Hình ảnh: </strong></p>
                        <p><img class="w-25" src="../images/${user.image}" alt=""></p>
                      </div>

                    </div>
                  </div>
                  <div class="row tab-pane active" id="home">
                    <div class="container mt-5">
                      <div class="row row-cols-1 row-cols-md-2 row-cols-xl-4">
                        <div class="col">
                          <div class="card radius-10 border-start border-0 border-3 border-info">
                            <div class="card-body">
                              <div class="d-flex align-items-center">
                                <div>
                                  <p class="mb-0 text-secondary text-danger" style="font-weight: bold;">Giỏ hàng</p>
                                  <h4 class="my-1 text-info">34.6%</h4>
                                  <p class="mb-0 font-13">+2.5% from last week</p>
                                </div>
                                <div class="widgets-icons-2 rounded-circle bg-gradient-scooter text-white ms-auto"><i
                                    class="fa fa-shopping-cart"></i>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col">
                          <a href="/ctdh/dangxuli/" class="text-decoration-none">
                            <div class="card radius-10 border-start border-0 border-3 border-danger">
                              <div class="card-body">
                                <div class="d-flex align-items-center">
                                  <div>
                                    <p class="mb-0 text-secondary text-danger" style="font-weight: bold;">Đang xử lí</p>
                                    <h4 class="my-1 text-danger">$84,245</h4>
                                    <p class="mb-0 font-13">+5.4% from last week</p>
                                  </div>
                                  <div class="widgets-icons-2 rounded-circle bg-gradient-bloody text-white ms-auto"><i
                                      class="fa fa-book"></i>
                                  </div>
                                </div>
                              </div>
                            </div>
                          </a>
                        </div>
                        <div class="col">
                          <div class="card radius-10 border-start border-0 border-3 border-success">
                            <div class="card-body">
                              <div class="d-flex align-items-center">
                                <div>
                                  <p class="mb-0 text-secondary text-danger" style="font-weight: bold;">Đã giao</p>
                                  <h4 class="my-1 text-success">34.6%</h4>
                                  <p class="mb-0 font-13">-4.5% from last week</p>
                                </div>
                                <div class="widgets-icons-2 rounded-circle bg-gradient-ohhappiness text-white ms-auto">
                                  <i class="fa fa-calendar-check-o"></i>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="col">
                          <div class="card radius-10 border-start border-0 border-3 border-warning">
                            <div class="card-body">
                              <div class="d-flex align-items-center">
                                <div>
                                  <p class="mb-0 text-secondary text-danger" style="font-weight: bold;">Đã hủy</p>
                                  <h4 class="my-1 text-warning">8.4K</h4>
                                  <p class="mb-0 font-13">+8.4% from last week</p>
                                </div>
                                <div class="widgets-icons-2 rounded-circle bg-gradient-blooker text-white ms-auto"><i
                                    class="fa fa-times-circle"></i>
                                </div>
                              </div>
                            </div>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
                  <div id="update" class="tab-pane">
                    <form action="/profile/information/" method="post" modelAttribute="user">
                      <div class="modal-content" style="background: linear-gradient(to bottom, #000428, #004683);">
                        <div class="modal-header">
                          <h5 class="modal-title text-center" id="staticBackdropLabel">Cập nhật thông tin</h5>
                        </div>
                        <div class="modal-body">
                          <div class="form-group row">
                            <div class="col-lg-6">
                              <label for="" class="col-lg-6 col-form-label">Họ và tên</label>
                              <div class="col-lg-12">
                                <input class="form-control" type="text" name="username" path="username" id=""
                                  value="${user.username}">
                              </div>
                            </div>
                            <div class="col-lg-6">
                              <label for="" class="col-lg-6 col-form-label">Email</label>
                              <div class="col-lg-12">
                                <input class="form-control" type="text" name="email" path="email" id=""
                                  value="${user.email}">
                              </div>
                            </div>
                          </div>
                          <div class="form-group row">
                            <div class="col-lg-6">
                              <label for="" class="col-lg-6 col-form-label">Số điện thoại</label>
                              <div class="col-lg-12">
                                <input class="form-control" type="text" name="sdt" path="sdt" id="" value="${user.sdt}">
                              </div>
                            </div>
                            <div class="col-lg-6">
                              <label for="" class="col-lg-6 col-form-label">Địa chỉ</label>
                              <div class="col-lg-12">
                                <input class="form-control" type="text" name="diachi" path="diachi" id=""
                                  value="${user.diachi}">
                              </div>
                            </div>
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button formaction="/profile/information/" type="submit" class="btn btn-primary">Cập
                            nhật</button>
                        </div>
                      </div>
                    </form>
                  </div>

                  <div class="tab-pane" id="mk">
                    <div class="modal-content" style="background: linear-gradient(to bottom, #000428, #004683);">
                      <form action="/profile/updatemk/" method="post">
                        <div class="modal-header">
                          <h5 class="modal-title text-center" id="staticBackdropLabel">Cập nhật thông tin</h5>
                        </div>
                        <div class="modal-body">
                          <div class="form-group row p-3">
                            <label for="" class="col-sm-4 col-form-label">Mật khẩu cũ</label>
                            <div class="col-sm-8">
                              <input class="form-control" type="password" name="mkc" id="" value="">
                            </div>
                          </div>
                          <div class="form-group row p-3">
                            <label for="" class="col-sm-4 col-form-label">Mật khẩu mới</label>
                            <div class="col-sm-8">
                              <input class="form-control" type="password" name="mkm" id="" value="">
                            </div>
                          </div>
                          <div class="form-group row p-3">
                            <label for="" class="col-sm-4 col-form-label">Xác nhận mật khẩu mới</label>
                            <div class="col-sm-8">
                              <input class="form-control" type="password" name="xnmk" id="" value="">
                            </div>
                          </div>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                          <button formaction="/profile/updatemk/" type="submit" class="btn btn-primary">Cập
                            nhật</button>
                        </div>
                      </form>
                    </div>
                  </div>
                  <!-- END PROFILE -->
                </div>
              </div>
            </div>
          </div>
        </div>
        <!-- END USER PROFILE -->
      </div>
      </div>
    </body>

    </html>