<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
            <!DOCTYPE html>
            <html>

            <head>
                <meta charset="utf-8">
                <title>Insert title here</title>
            </head>

            <body>
                <!-- Carousel Start -->
                <div class="container-fluid mb-3">
                    <div class="row px-xl-5">
                        <div class="col-lg-8">
                            <div id="header-carousel" class="carousel slide carousel-fade mb-30 mb-lg-0"
                                data-ride="carousel">
                                <ol class="carousel-indicators">
                                    <li data-target="#header-carousel" data-slide-to="0" class="active"></li>
                                    <li data-target="#header-carousel" data-slide-to="1"></li>
                                    <li data-target="#header-carousel" data-slide-to="2"></li>
                                </ol>
                                <div class="carousel-inner">
                                    <div class="carousel-item position-relative active" style="height: 430px;">
                                        <img class="position-absolute w-100 h-100" src="../images/logo2.png"
                                            style="object-fit: cover;">
                                        <div
                                            class="carousel-caption d-flex flex-column align-items-center justify-content-center">
                                            <div class="p-3" style="max-width: 700px;">
                                                <h1
                                                    class="display-4 text-white mb-3 animate__animated animate__fadeInDown">
                                                    Modern Laptops</h1>
                                                <p class="mx-md-5 px-5 animate__animated animate__bounceIn">Lorem rebum
                                                    magna
                                                    amet
                                                    lorem magna erat diam stet. Sadips duo stet amet amet ndiam elitr
                                                    ipsum
                                                    diam
                                                </p>
                                                <a class="btn btn-outline-light py-2 px-4 mt-3 animate__animated animate__fadeInUp"
                                                    href="#">Shop Now</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="carousel-item position-relative" style="height: 430px;">
                                        <img class="position-absolute w-100 h-100" src="../images/logo1.png"
                                            style="object-fit: cover;">
                                        <div
                                            class="carousel-caption d-flex flex-column align-items-center justify-content-center">
                                            <div class="p-3" style="max-width: 700px;">
                                                <h1
                                                    class="display-4 text-white mb-3 animate__animated animate__fadeInDown">
                                                    Women
                                                    Fashion</h1>
                                                <p class="mx-md-5 px-5 animate__animated animate__bounceIn">Lorem rebum
                                                    magna
                                                    amet
                                                    lorem magna erat diam stet. Sadips duo stet amet amet ndiam elitr
                                                    ipsum
                                                    diam
                                                </p>
                                                <a class="btn btn-outline-light py-2 px-4 mt-3 animate__animated animate__fadeInUp"
                                                    href="#">Shop Now</a>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="carousel-item position-relative" style="height: 430px;">
                                        <img class="position-absolute w-100 h-100" src="../images/logo4.png"
                                            style="object-fit: cover;">
                                        <div
                                            class="carousel-caption d-flex flex-column align-items-center justify-content-center">
                                            <div class="p-3" style="max-width: 700px;">
                                                <h1
                                                    class="display-4 text-white mb-3 animate__animated animate__fadeInDown">
                                                    Kids
                                                    Fashion</h1>
                                                <p class="mx-md-5 px-5 animate__animated animate__bounceIn">Lorem rebum
                                                    magna
                                                    amet
                                                    lorem magna erat diam stet. Sadips duo stet amet amet ndiam elitr
                                                    ipsum
                                                    diam
                                                </p>
                                                <a class="btn btn-outline-light py-2 px-4 mt-3 animate__animated animate__fadeInUp"
                                                    href="#">Shop Now</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-lg-4">
                            <div class="product-offer mb-30" style="height: 200px;">
                                <img class="img-fluid" src="../images/logo3.png" alt="">
                                <div class="offer-text">
                                    <h6 class="text-white text-uppercase">Save 20%</h6>
                                    <h3 class="text-white mb-3">Special Offer</h3>
                                    <a href="" class="btn btn-primary">Shop Now</a>
                                </div>
                            </div>
                            <div class="product-offer mb-30" style="height: 200px;">
                                <img class="img-fluid" src="../images/logo5.png" alt="">
                                <div class="offer-text">
                                    <h6 class="text-white text-uppercase">Save 20%</h6>
                                    <h3 class="text-white mb-3">Special Offer</h3>
                                    <a href="" class="btn btn-primary">Shop Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Carousel End -->


                <!-- Featured Start -->
                <div class="container-fluid pt-5">
                    <div class="row px-xl-5 pb-3">
                        <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
                            <div class="d-flex align-items-center bg-light mb-4" style="padding: 30px;">
                                <h1 class="fa fa-check text-primary m-0 mr-3"></h1>
                                <h5 class="font-weight-semi-bold m-0">Quality Product</h5>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
                            <div class="d-flex align-items-center bg-light mb-4" style="padding: 30px;">
                                <h1 class="fa fa-shipping-fast text-primary m-0 mr-2"></h1>
                                <h5 class="font-weight-semi-bold m-0">Free Shipping</h5>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
                            <div class="d-flex align-items-center bg-light mb-4" style="padding: 30px;">
                                <h1 class="fas fa-exchange-alt text-primary m-0 mr-3"></h1>
                                <h5 class="font-weight-semi-bold m-0">14-Day Return</h5>
                            </div>
                        </div>
                        <div class="col-lg-3 col-md-6 col-sm-12 pb-1">
                            <div class="d-flex align-items-center bg-light mb-4" style="padding: 30px;">
                                <h1 class="fa fa-phone-volume text-primary m-0 mr-3"></h1>
                                <h5 class="font-weight-semi-bold m-0">24/7 Support</h5>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Featured End -->


                <!-- Categories Start -->
                <div class="container-fluid pt-5">
                    <h2 class="section-title position-relative text-uppercase mx-xl-5 mb-4"><span
                            class="bg-secondary pr-3">Categories</span></h2>
                    <div class="row px-xl-5 pb-3">
                        <c:forEach items="${th}" var="th" begin="0" end="15">
                            <div class="col-lg-3 col-md-4 col-sm-6 pb-1">
                                <a class="text-decoration-none" href="/category/${th.thid}">
                                    <div class="cat-item d-flex align-items-center mb-4">
                                        <div class="overflow-hidden" style="width: 150px; height: 90px;">
                                            <img class="w-100 h-100" src="${th.logo}" alt=""
                                                style="object-fit: scale-down;">
                                        </div>
                                        <div class="flex-fill pl-3">
                                            <h6>${th.tenth}</h6>
                                            <small class="text-body">100 Products Laptop</small>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </c:forEach>
                    </div>
                </div>
                <!-- Categories End -->


                <!-- Products Start -->
                <div class="container-fluid pt-5 pb-3">
                    <h2 class="section-title position-relative text-uppercase mx-xl-5 mb-4"><span
                            class="bg-secondary pr-3">Featured
                            Products</span></h2>
                    <div class="row px-xl-5">
                        <c:set var="maxItems" value="8" />
                        <c:forEach items="${sp}" var="sp" varStatus="status">
                            <c:if test="${status.index < maxItems}">
                                <div class="col-lg-3 col-md-4 col-sm-6 pb-1">
                                    <div class="product-item bg-light mb-4">
                                        <div class="product-img position-relative overflow-hidden">
                                            <img class="img-fluid w-100" src="../images/${sp.hinh}" alt="">
                                            <div class="product-action">
                                                <a class="btn btn-outline-dark btn-square" href="/cart/${sp.spid}"><i
                                                        class="fa fa-shopping-cart"></i></a>
                                                <a class="btn btn-outline-dark btn-square" href=""><i
                                                        class="far fa-heart"></i></a>
                                                <a class="btn btn-outline-dark btn-square" href=""><i
                                                        class="fa fa-sync-alt"></i></a>
                                                <a class="btn btn-outline-dark btn-square" href=""><i
                                                        class="fa fa-search"></i></a>
                                            </div>
                                        </div>
                                        <div class="text-center py-4" style="height: 316px;">
                                            <a class="h6 text-decoration-none text-truncate" href="/ctsp/${sp.spid}">
                                                <p>
                                                    ${sp.tensp}
                                                </p>
                                            </a>
                                            <div class="d-flex align-items-center justify-content-center mt-2">
                                                <h5>
                                                    <fmt:setLocale value="vi_VN" />
                                                    <fmt:formatNumber value="${sp.gia}" type="number" pattern="#,##0" />
                                                </h5><span> đ</span>
                                                <h6 class="text-muted ml-2">
                                                    <del>
                                                        <fmt:setLocale value="vi_VN" />
                                                        <fmt:formatNumber value="${sp.giacu}" type="number"
                                                            pattern="#,##0" /><span> đ</span>
                                                    </del>
                                                </h6>
                                            </div>
                                            <div class="d-flex align-items-center justify-content-center mb-1">
                                                <small class="fa fa-star text-primary mr-1"></small>
                                                <small class="fa fa-star text-primary mr-1"></small>
                                                <small class="fa fa-star text-primary mr-1"></small>
                                                <small class="fa fa-star text-primary mr-1"></small>
                                                <small class="fa fa-star text-primary mr-1"></small>
                                                <small>(99)</small>
                                            </div>
                                            <div class="d-flex align-items-center justify-content-start mt-2 ml-3">
                                                <p>Màn hình: <span>${sp.manhinh}</span></p>
                                            </div>
                                            <div class="d-flex align-items-center justify-content-start mt-2 ml-3">
                                                <p>CPU: <span>${sp.cpu}</span></p>
                                            </div>
                                            <div class="d-flex align-items-center justify-content-start mt-2 ml-3">
                                                <p>Pin: <span>${sp.thongtinpin}</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:if>
                        </c:forEach>
                    </div>
                </div>
                <!-- Products End -->


                <!-- Offer Start -->
                <div class="container-fluid pt-5 pb-3">
                    <div class="row px-xl-5">
                        <div class="col-md-6">
                            <div class="product-offer mb-30" style="height: 300px;">
                                <img class="img-fluid" src="../images/logo7.png" alt="">
                                <div class="offer-text">
                                    <h6 class="text-white text-uppercase">Save 20%</h6>
                                    <h3 class="text-white mb-3">Special Offer</h3>
                                    <a href="" class="btn btn-primary">Shop Now</a>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="product-offer mb-30" style="height: 300px;">
                                <img class="img-fluid" src="../images/logo6.png" alt="">
                                <div class="offer-text">
                                    <h6 class="text-white text-uppercase">Save 20%</h6>
                                    <h3 class="text-white mb-3">Special Offer</h3>
                                    <a href="" class="btn btn-primary">Shop Now</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Offer End -->


                <!-- Products Start -->
                <div class="container-fluid pt-5 pb-3">
                    <h2 class="section-title position-relative text-uppercase mx-xl-5 mb-4"><span
                            class="bg-secondary pr-3">Recent
                            Products</span></h2>
                    <div class="row px-xl-5">
                        <c:forEach items="${sp}" var="sp" begin="4" end="12">
                            <div class="col-lg-3 col-md-4 col-sm-6 pb-1">
                                <div class="product-item bg-light mb-4">
                                    <div class="product-img position-relative overflow-hidden">
                                        <img class="img-fluid w-100" src="../images/${sp.hinh}" alt="">
                                        <div class="product-action">
                                            <a class="btn btn-outline-dark btn-square" href="/cart/${sp.spid}"><i
                                                    class="fa fa-shopping-cart"></i></a>
                                            <a class="btn btn-outline-dark btn-square" href=""><i
                                                    class="far fa-heart"></i></a>
                                            <a class="btn btn-outline-dark btn-square" href=""><i
                                                    class="fa fa-sync-alt"></i></a>
                                            <a class="btn btn-outline-dark btn-square" href=""><i
                                                    class="fa fa-search"></i></a>
                                        </div>
                                    </div>
                                    <div class="text-center py-4" style="height: 316px;">
                                        <a class="h6 text-decoration-none text-truncate" href="/ctsp/${sp.spid}">
                                            <p>
                                                ${sp.tensp}
                                            </p>
                                        </a>
                                        <div class="d-flex align-items-center justify-content-center mt-2">
                                            <h5>
                                                <fmt:setLocale value="vi_VN" />
                                                <fmt:formatNumber value="${sp.gia}" type="number" pattern="#,##0" />
                                            </h5><span> đ</span>
                                            <h6 class="text-muted ml-2">
                                                <del>
                                                    <fmt:setLocale value="vi_VN" />
                                                    <fmt:formatNumber value="${sp.giacu}" type="number"
                                                        pattern="#,##0" /><span> đ</span>
                                                </del>
                                            </h6>
                                        </div>
                                        <div class="d-flex align-items-center justify-content-center mb-1">
                                            <small class="fa fa-star text-primary mr-1"></small>
                                            <small class="fa fa-star text-primary mr-1"></small>
                                            <small class="fa fa-star text-primary mr-1"></small>
                                            <small class="fa fa-star text-primary mr-1"></small>
                                            <small class="fa fa-star text-primary mr-1"></small>
                                            <small>(99)</small>
                                        </div>
                                        <div class="d-flex align-items-center justify-content-start mt-2 ml-3">
                                            <p>Màn hình: <span>${sp.manhinh}</span></p>
                                        </div>
                                        <div class="d-flex align-items-center justify-content-start mt-2 ml-3">
                                            <p>CPU: <span>${sp.cpu}</span></p>
                                        </div>
                                        <div class="d-flex align-items-center justify-content-start mt-2 ml-3">
                                            <p>Pin: <span>${sp.thongtinpin}</span></p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </c:forEach>
                    </div>
                </div>
                <!-- Products End -->


                <!-- Vendor Start -->
                <div class="container-fluid py-5">
                    <div class="row px-xl-5">
                        <div class="col">
                            <div class="owl-carousel vendor-carousel">
                                <div class="bg-light p-4">
                                    <img src="../images/vendor-1.jpg" alt="">
                                </div>
                                <div class="bg-light p-4">
                                    <img src="../images/vendor-2.jpg" alt="">
                                </div>
                                <div class="bg-light p-4">
                                    <img src="../images/vendor-3.jpg" alt="">
                                </div>
                                <div class="bg-light p-4">
                                    <img src="../images/vendor-4.jpg" alt="">
                                </div>
                                <div class="bg-light p-4">
                                    <img src="../images/vendor-5.jpg" alt="">
                                </div>
                                <div class="bg-light p-4">
                                    <img src="../images/vendor-6.jpg" alt="">
                                </div>
                                <div class="bg-light p-4">
                                    <img src="../images/vendor-7.jpg" alt="">
                                </div>
                                <div class="bg-light p-4">
                                    <img src="../images/vendor-8.jpg" alt="">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Vendor End -->
            </body>

            </html>