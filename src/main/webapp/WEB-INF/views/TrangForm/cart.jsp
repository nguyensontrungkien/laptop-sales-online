<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
            <!DOCTYPE html>
            <html>

            <head>
                <meta charset="utf-8">
                <title>Insert title here</title>
                <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
                <script>
                    $(document).ready(function () {
                        $('#selectAllCheckbox').change(function () {
                            var isChecked = $(this).prop('checked');
                            $('input[type="checkbox"]').prop('checked', isChecked);
                            updateTotalPrice();
                        });

                        $('input[type="checkbox"]').change(function () {
                            updateTotalPrice();
                            updateSelectedCount();
                        });

                        function updateTotalPrice() {
                            var totalPrice = 0;
                            $('input[type="checkbox"]:checked').each(function () {
                                var price = parseFloat($(this).closest('tr').find('.total').text());
                                totalPrice += price;
                            });
                            var total = totalPrice.toLocaleString('vi-VN', {
                                style: 'currency',
                                currency: 'VND'
                            });

                            $('#totalPrice').text(total);
                        }

                        function updateSelectedCount() {
                            var count = $('input[name="check"]:checked').length;
                            $('#selectedCount').text(count);
                        }
                    });
                </script>
            </head>

            <body>
                <!-- Breadcrumb Start -->
                <div class="container-fluid">
                    <div class="row px-xl-5">
                        <div class="col-12">
                            <nav class="breadcrumb bg-light mb-30">
                                <a class="breadcrumb-item text-dark" href="#">Home</a>
                                <a class="breadcrumb-item text-dark" href="#">Shop</a>
                                <span class="breadcrumb-item active">Shopping Cart</span>
                            </nav>
                        </div>
                    </div>
                </div>
                <!-- Breadcrumb End -->


                <!-- Cart Start -->
                <div class="container-fluid">
                    <form action="/cart/" method="post">
                        <div class="row px-xl-5">
                            <div class="col-lg-8 table-responsive mb-5">
                                <table class="table table-light table-borderless table-hover text-center mb-0">
                                    <thead class="thead-dark">
                                        <tr>
                                            <th></th>
                                            <th>Products</th>
                                            <th>Image</th>
                                            <th>Price</th>
                                            <th>Quantity</th>
                                            <th>Total</th>
                                            <th>Remove</th>
                                        </tr>
                                    </thead>
                                    <tbody class="align-middle">
                                        <c:forEach var="gh" items="${gh}">
                                            <tr>
                                                <td><input type="checkbox" name="check" value="${gh.ghid}"></td>
                                                <td class="align-middle">${gh.sanpham.tensp}</td>
                                                <td><img src="../images/${gh.sanpham.hinh}" alt=""
                                                        style="width: 100px;">
                                                </td>
                                                <td class="align-middle">
                                                    <fmt:setLocale value="vi_VN" />
                                                    <fmt:formatNumber value="${gh.sanpham.gia}" type="number"
                                                        pattern="#,##0" /><span> đ</span>
                                                </td>
                                                <form action="" method="post" modelAttribute="gh1">
                                                    <td class="align-middle">
                                                        <div class="input-group quantity mx-auto" style="width: 100px;">
                                                            <div class="input-group-btn">
                                                                <button formaction="/cart/update/${gh.ghid}"
                                                                    class="btn btn-sm btn-primary btn-minus">
                                                                    <i class="fa fa-minus"></i>
                                                                </button>
                                                            </div>
                                                            <input type="text"
                                                                class="form-control form-control-sm bg-secondary border-0 text-center"
                                                                value="${gh.soluong}" min="1" name="soluong"
                                                                path="soluong">
                                                            <div class="input-group-btn">
                                                                <button formaction="/cart/update/${gh.ghid}"
                                                                    class="btn btn-sm btn-primary btn-plus">
                                                                    <i class="fa fa-plus"></i>
                                                                </button>
                                                            </div>
                                                        </div>
                                                    </td>
                                                    <td class="align-middle total">${gh.tong}
                                                    </td>
                                                </form>
                                                <td class="align-middle">

                                                    <button formaction="/cart/delete/${gh.ghid}"
                                                        class="btn btn-sm btn-danger">
                                                        <i class="fa fa-times"></i>
                                                    </button>

                                                </td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>
                            </div>
                            <div class="col-lg-4">
                                <form class="mb-30" action="">
                                    <div class="input-group">
                                        <input type="text" class="form-control border-0 p-4" placeholder="Coupon Code">
                                        <div class="input-group-append">
                                            <button class="btn btn-primary">Apply Coupon</button>
                                        </div>
                                    </div>
                                </form>
                                <h5 class="section-title position-relative text-uppercase mb-3"><span
                                        class="bg-secondary pr-3">Cart Summary</span></h5>
                                <div class="bg-light p-30 mb-5">
                                    <div class="border-bottom pb-2">
                                        <div class="d-flex justify-content-between mb-3">
                                            <h6>Subtotal</h6>
                                            <h6>$150</h6>
                                        </div>
                                        <div class="d-flex justify-content-between">
                                            <h6 class="font-weight-medium">Shipping</h6>
                                            <h6 class="font-weight-medium">$10</h6>
                                        </div>
                                    </div>
                                    <div class="pt-2">
                                        <div class="d-flex justify-content-between mt-2">
                                            <h5>Total</h5>
                                            <h5 name="total2" id="totalPrice"></h5>
                                            <h5 id="selectedCount">0.0</h5>
                                        </div>
                                        <button formaction="/bill/"
                                            class="btn btn-block btn-primary font-weight-bold my-3 py-3">Proceed To
                                            Checkout</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>

                <!-- Cart End -->
            </body>

            </html>