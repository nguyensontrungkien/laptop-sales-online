<%@ page language="java" contentType="text/html; charset=ISO-8859-1" pageEncoding="ISO-8859-1" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
            <!DOCTYPE html>
            <html>

            <head>
                <meta charset="ISO-8859-1">
                <title>Insert title here</title>
            </head>

            <body>
                <!-- Categories Start -->
                <div class="container-fluid pt-5">
                    <h2 class="section-title position-relative text-uppercase mx-xl-5 mb-4"><span
                            class="bg-secondary pr-3">Categories</span></h2>
                    <div class="row px-xl-5 pb-3">
                        <c:forEach items="${th}" var="th" begin="0" end="15">
                            <div class="col-lg-3 col-md-4 col-sm-6 pb-1">
                                <a class="text-decoration-none" href="/category/${th.thid}">
                                    <div class="cat-item d-flex align-items-center mb-4">
                                        <div class="overflow-hidden" style="width: 150px; height: 90px;">
                                            <img class="w-100 h-100" src="${th.logo}" alt=""
                                                style="object-fit: scale-down;">
                                        </div>
                                        <div class="flex-fill pl-3">
                                            <h6>${th.tenth}</h6>
                                            <small class="text-body">100 Products Laptop</small>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </c:forEach>
                    </div>
                </div>
                <!-- Categories End -->


                <!-- Products Start -->
                <div class="container-fluid pt-5 pb-3">
                    <h2 class="section-title position-relative text-uppercase mx-xl-5 mb-4"><span
                            class="bg-secondary pr-3">TRADEMARK ${thsp.tenth}</span></h2>
                    <div class="row px-xl-5">
                        <c:set var="maxItems" value="8" />
                        <c:forEach items="${thsp.sanpham}" var="thsp" varStatus="status">
                            <c:if test="${status.index < maxItems}">
                                <div class="col-lg-3 col-md-4 col-sm-6 pb-1">
                                    <div class="product-item bg-light mb-4">
                                        <div class="product-img position-relative overflow-hidden">
                                            <img class="img-fluid w-100" src="../images/${thsp.hinh}" alt="">
                                            <div class="product-action">
                                                <a class="btn btn-outline-dark btn-square" href="/cart/${thsp.spid}"><i
                                                        class="fa fa-shopping-cart"></i></a>
                                                <a class="btn btn-outline-dark btn-square" href=""><i
                                                        class="far fa-heart"></i></a>
                                                <a class="btn btn-outline-dark btn-square" href=""><i
                                                        class="fa fa-sync-alt"></i></a>
                                                <a class="btn btn-outline-dark btn-square" href=""><i
                                                        class="fa fa-search"></i></a>
                                            </div>
                                        </div>
                                        <div class="text-center py-4" style="height: 316px;">
                                            <a class="h6 text-decoration-none text-truncate" href="/ctsp/${thsp.spid}">
                                                <p>
                                                    ${thsp.tensp}
                                                </p>
                                            </a>
                                            <div class="d-flex align-items-center justify-content-center mt-2">
                                                <h5>
                                                    <fmt:setLocale value="vi_VN" />
                                                    <fmt:formatNumber value="${thsp.gia}" type="number"
                                                        pattern="#,##0" />
                                                </h5><span> đ</span>
                                                <h6 class="text-muted ml-2">
                                                    <del>
                                                        <fmt:setLocale value="vi_VN" />
                                                        <fmt:formatNumber value="${thsp.giacu}" type="number"
                                                            pattern="#,##0" />
                                                        <span> đ</span>
                                                    </del>
                                                </h6>
                                            </div>
                                            <div class="d-flex align-items-center justify-content-center mb-1">
                                                <small class="fa fa-star text-primary mr-1"></small>
                                                <small class="fa fa-star text-primary mr-1"></small>
                                                <small class="fa fa-star text-primary mr-1"></small>
                                                <small class="fa fa-star text-primary mr-1"></small>
                                                <small class="fa fa-star text-primary mr-1"></small>
                                                <small>(99)</small>
                                            </div>
                                            <div class="d-flex align-items-center justify-content-start mt-2 ml-3">
                                                <p>Màn hình: <span>${thsp.manhinh}</span></p>
                                            </div>
                                            <div class="d-flex align-items-center justify-content-start mt-2 ml-3">
                                                <p>CPU: <span>${thsp.cpu}</span></p>
                                            </div>
                                            <div class="d-flex align-items-center justify-content-start mt-2 ml-3">
                                                <p>Pin: <span>${thsp.thongtinpin}</span></p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:if>
                        </c:forEach>
                    </div>
                </div>
                <!-- Products End -->

            </body>

            </html>