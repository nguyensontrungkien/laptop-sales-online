<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8">
        <title>Insert title here</title>
        <link rel="stylesheet" href="../css/qlsp.css">
    </head>

    <body>
        <div class="container-fluid px-1 py-3 mx-auto">
            <div class="row d-flex justify-content-center">
                <div class="col-xl-12 col-lg-12 col-md-12 col-lg-12 text-center">
                    <h3>FORM QUẢN LÝ THƯƠNG HIỆU</h3>
                    <p class="blue-text">Xin hãy điền đầy đủ thông tin trong form trước khi dùng các chức năng !!!</p>
                    <div class="card w-100">
                        <h5 class="text-center mb-4">QUẢN LÝ THƯƠNG HIỆU</h5>
                        <form class="form-card" action="/trangchuad/2" method="post" modelAttribute="th">
                            <div class=" row justify-content-between text-left ">
                                <div class="form-group col-sm-3 flex-column d-flex">
                                    <label class="form-control-label px-3">
                                        <span class="text-info font-weight-bold">Mã Thương Hiệu</span>
                                    </label>
                                    <input class="input" type="text" id="fname" name="thid" path="thid"
                                        value="${th.thid}" placeholder="Mã Thương Hiệu">
                                </div>
                                <div class="form-group col-sm-3 flex-column d-flex">
                                    <label class="form-control-label px-3">
                                        <span class="text-info font-weight-bold">Tên Thương Hiệu</span>
                                    </label>
                                    <input class="input" type="text" id="fname" name="tenth" path="tenth"
                                        value="${th.tenth}" placeholder="Tên Thương Hiệu">
                                </div>
                                <div class="form-group col-sm-3 flex-column d-flex">
                                    <label class="form-control-label px-3">
                                        <span class="text-info font-weight-bold">Logo</span>
                                    </label>
                                    <input class="input" type="text" id="fname" name="logo" path="logo"
                                        value="${th.logo}" placeholder="Logo">
                                </div>

                            </div>

                            <div class="row justify-content-end">
                                <div class="form-group col-sm-3">
                                    <button formaction="/trangchuad/2/insert" type="submit"
                                        class="btn-block btn-primary button1">Thêm</button>
                                </div>
                                <div class="form-group col-sm-3">
                                    <button formaction="/trangchuad/2/update" type="submit"
                                        class="btn-block btn-primary button1">Sửa</button>
                                </div>
                                <div class="form-group col-sm-3">
                                    <button formaction="/trangchuad/2/delete/${th.thid}"
                                        class="btn-block btn-primary button1">Xóa</button>
                                </div>
                                <div class="form-group col-sm-3">
                                    <button formaction="/trangchuad/{check}/clean" type="submit"
                                        class="btn-block btn-primary button1">Làm mới</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>

    </html>