<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
  <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
    <!DOCTYPE html>
    <html>

    <head>
      <meta charset="utf-8">
      <title>Insert title here</title>
      <link rel="stylesheet" href="../css/admin.css">
      <style>

      </style>
    </head>

    <body>

      <div class="row home">
        <div class="col-lg-3 bg-info py-5">
          <ul>
            <a class="text-decoration-none" href="/trangchuad/0">
              <li class="h2 list-unstyled">HOME</li>
            </a>
            <a class="text-decoration-none" href="">
              <li class="h2 list-unstyled">THỐNG KÊ</li>
            </a>
            <a class="text-decoration-none" href="/trangchuad/1">
              <li class="h2 list-unstyled">QL SẢN PHẨM</li>
            </a>
            <a class="text-decoration-none" href="/trangchuad/2">
              <li class="h2 list-unstyled">QL THƯƠNG HIỆU</li>
            </a>
            <a class="text-decoration-none" href="">
              <li class="h2 list-unstyled">QL NGƯỜI DÙNG</li>
            </a>
          </ul>
        </div>
        <div class="col-lg-9">
          <!-- home -->
          <c:if test="${check =='0'}">
            <img style="width: 1100px;height: 600px;" alt="" src="../images/logozuhot.png">
          </c:if>
          <!-- qlsp -->
          <c:if test="${check =='1'}">
            <jsp:include page="./qlspForm.jsp"></jsp:include>
          </c:if>
          <!-- qlth -->
          <c:if test="${check =='2'}">
            <jsp:include page="./qlthForm.jsp"></jsp:include>
          </c:if>
        </div>
      </div>
      <div class="row">
        <!-- qlsp -->
        <c:if test="${check =='1'}">
          <jsp:include page="./qlsp.jsp"></jsp:include>
        </c:if>
        <!-- qlth -->
        <c:if test="${check =='2'}">
          <jsp:include page="./qlth.jsp"></jsp:include>
        </c:if>
      </div>
    </body>

    </html>