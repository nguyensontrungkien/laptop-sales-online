<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
    <!DOCTYPE html>
    <html>

    <head>
        <meta charset="utf-8">
        <title>Insert title here</title>
        <link rel="stylesheet" href="../css/qlsp.css">
    </head>

    <body>
        <div class="container-fluid px-1 py-3 mx-auto">
            <div class="row d-flex justify-content-center">
                <div class="col-xl-12 col-lg-12 col-md-12 col-lg-12 text-center">
                    <h3>FORM QUẢN LÝ SẢN PHẨM</h3>
                    <p class="blue-text">Xin hãy điền đầy đủ thông tin trong form trước khi dùng các chức năng !!!</p>
                    <div class="card w-100">
                        <h5 class="text-center mb-4">QUẢN LÝ SẢN PHẨM</h5>
                        <form class="form-card">
                            <div class=" row justify-content-between text-left ">
                                <div class="form-group col-sm-3 flex-column d-flex">
                                    <label class="form-control-label px-3">
                                        <span class="text-info font-weight-bold">Tên sản phẩm</span>
                                    </label>
                                    <input class="input" type="text" id="fname" name="fname" placeholder="Tên sản phẩm">
                                </div>
                                <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                    <label class="form-control-label px-3">
                                        <span class="text-info font-weight-bold danger">Thương hiệu</span>
                                    </label>
                                    <select class="form-control rounded mt-1" name="thuonghieu" id="">
                                        <option value="">ASUS</option>
                                        <option value="">LENOVO</option>
                                        <option value="">RAZER</option>
                                        <option value="">ACER</option>
                                        <option value="">DELL</option>
                                        <option value="">MICROSOFT</option>
                                        <option value="">HP</option>
                                        <option value="">MSI</option>
                                        <option value="">SAMSUNG</option>
                                        <option value="">ALIENWARE</option>
                                        <option value="">HUAWEI</option>
                                        <option value="">GIGABYTE</option>
                                        <option value="">LG</option>
                                        <option value="">THX</option>
                                        <option value="">APPLE</option>
                                        <option value="">VICTUS</option>
                                    </select>
                                </div>
                                <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                    <label class="form-control-label px-3">
                                        <span class="text-info font-weight-bold">Giá</span>
                                    </label>
                                    <input class="input" type="text" id="lname" name="lname"
                                        placeholder="Enter your last name">
                                </div>
                                <div class="form-group col-sm-3 flex-column d-flex">
                                    <label class="form-control-label px-3">
                                        <span class="text-info font-weight-bold">Giá cũ</span>
                                    </label>
                                    <input class="input" type="text" id="fname" name="fname" placeholder="Tên sản phẩm">
                                </div>
                                <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                    <label class="form-control-label px-3">
                                        <span class="text-info font-weight-bold">Hình</span>
                                    </label>
                                    <input class="input" type="text" id="lname" name="lname"
                                        placeholder="Enter your last name">
                                </div>
                                <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                    <label class="form-control-label px-3">
                                        <span class="text-info font-weight-bold">Màu sắc</span>
                                    </label>
                                    <input class="input" type="text" id="lname" name="lname"
                                        placeholder="Enter your last name">
                                </div>
                                <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                    <label class="form-control-label px-3">
                                        <span class="text-info font-weight-bold">CPU</span>
                                    </label>
                                    <input class="input" type="text" id="lname" name="lname"
                                        placeholder="Enter your last name">
                                </div>
                                <div class="form-group col-sm-3 flex-column d-flex">
                                    <label class="form-control-label px-3">
                                        <span class="text-info font-weight-bold">Số nhân</span>
                                    </label>
                                    <input class="input" type="text" id="fname" name="fname" placeholder="Tên sản phẩm">
                                </div>
                                <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                    <label class="form-control-label px-3">
                                        <span class="text-info font-weight-bold">Số luồng</span>
                                    </label>
                                    <input class="input" type="text" id="lname" name="lname"
                                        placeholder="Enter your last name">
                                </div>
                                <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                    <label class="form-control-label px-3">
                                        <span class="text-info font-weight-bold">Tốc độ CPU</span>
                                    </label>
                                    <input class="input" type="text" id="lname" name="lname"
                                        placeholder="Enter your last name">
                                </div>
                                <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                    <label class="form-control-label px-3">
                                        <span class="text-info font-weight-bold">Tốc độ tối đa</span>
                                    </label>
                                    <input class="input" type="text" id="lname" name="lname"
                                        placeholder="Enter your last name">
                                </div>
                                <div class="form-group col-sm-3 flex-column d-flex">
                                    <label class="form-control-label px-3">
                                        <span class="text-info font-weight-bold">Ram</span>
                                    </label>
                                    <input class="input" type="text" id="fname" name="fname" placeholder="Tên sản phẩm">
                                </div>
                                <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                    <label class="form-control-label px-3">
                                        <span class="text-info font-weight-bold">Loại Ram</span>
                                    </label>
                                    <input class="input" type="text" id="lname" name="lname"
                                        placeholder="Enter your last name">
                                </div>
                                <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                    <label class="form-control-label px-3">
                                        <span class="text-info font-weight-bold">Tốc độ bus Ram</span>
                                    </label>
                                    <input class="input" type="text" id="lname" name="lname"
                                        placeholder="Enter your last name">
                                </div>
                                <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                    <label class="form-control-label px-3">
                                        <span class="text-info font-weight-bold">Hỗ trợ Ram tối
                                            đa</span>
                                    </label>
                                    <input class="input" type="text" id="lname" name="lname"
                                        placeholder="Enter your last name">
                                </div>
                                <div class="form-group col-sm-3 flex-column d-flex">
                                    <label class="form-control-label px-3">
                                        <span class="text-info font-weight-bold">Rom</span>
                                    </label>
                                    <input class="input" type="text" id="fname" name="fname" placeholder="Tên sản phẩm">
                                </div>
                                <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                    <label class="form-control-label px-3">
                                        <span class="text-info font-weight-bold">Màn hình</span>
                                    </label>
                                    <input class="input" type="text" id="lname" name="lname"
                                        placeholder="Enter your last name">
                                </div>
                                <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                    <label class="form-control-label px-3">
                                        <span class="text-info font-weight-bold">Độ phân giải</span>
                                    </label>
                                    <input class="input" type="text" id="lname" name="lname"
                                        placeholder="Enter your last name">
                                </div>
                                <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                    <label class="form-control-label px-3">
                                        <span class="text-info font-weight-bold">Tần số quét</span>
                                    </label>
                                    <input class="input" type="text" id="lname" name="lname"
                                        placeholder="Enter your last name">
                                </div>
                                <div class="form-group col-sm-3 flex-column d-flex">
                                    <label class="form-control-label px-3">
                                        <span class="text-info font-weight-bold">Công nghệ màn
                                            hình</span>
                                    </label>
                                    <input class="input" type="text" id="fname" name="fname" placeholder="Tên sản phẩm">
                                </div>
                                <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                    <label class="form-control-label px-3">
                                        <span class="text-info font-weight-bold">Card màn hình</span>
                                    </label>
                                    <input class="input" type="text" id="lname" name="lname"
                                        placeholder="Enter your last name">
                                </div>
                                <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                    <label class="form-control-label px-3">
                                        <span class="text-info font-weight-bold">Công nghệ âm
                                            thanh</span>
                                    </label>
                                    <input class="input" type="text" id="lname" name="lname"
                                        placeholder="Enter your last name">
                                </div>
                                <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                    <label class="form-control-label px-3">
                                        <span class="text-info font-weight-bold">Tính năng khác</span>
                                    </label>
                                    <input class="input" type="text" id="lname" name="lname"
                                        placeholder="Enter your last name">
                                </div>
                                <div class="form-group col-sm-3 flex-column d-flex">
                                    <label class="form-control-label px-3">
                                        <span class="text-info font-weight-bold">Đèn bàn phím</span>
                                    </label>
                                    <input class="input" type="text" id="fname" name="fname" placeholder="Tên sản phẩm">
                                </div>
                                <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                    <label class="form-control-label px-3">
                                        <span class="text-info font-weight-bold">Hệ điều hành</span>
                                    </label>
                                    <input class="input" type="text" id="lname" name="lname"
                                        placeholder="Enter your last name">
                                </div>
                                <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                    <label class="form-control-label px-3">
                                        <span class="text-info font-weight-bold">Thông tin Pin</span>
                                    </label>
                                    <input class="input" type="text" id="lname" name="lname"
                                        placeholder="Enter your last name">
                                </div>
                                <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                    <label class="form-control-label px-3">
                                        <span class="text-info font-weight-bold">Thiết kế</span>
                                    </label>
                                    <input class="input" type="text" id="lname" name="lname"
                                        placeholder="Enter your last name">
                                </div>
                                <div class="form-group col-sm-3 flex-column d-flex">
                                    <label class="form-control-label px-3">
                                        <span class="text-info font-weight-bold">Ngày ra bán</span>
                                    </label>
                                    <input class="input" type="text" id="fname" name="fname" placeholder="Tên sản phẩm">
                                </div>
                                <div class="form-group col-sm-3 flex-column d-flex text-dark">
                                    <label class="form-control-label px-3">
                                        <span class="text-info font-weight-bold">Kích thước</span>
                                    </label>
                                    <input class="input" type="text" id="lname" name="lname"
                                        placeholder="Enter your last name">
                                </div>
                            </div>

                            <div class="row justify-content-end">
                                <div class="form-group col-sm-3">
                                    <button type="submit" class="btn-block btn-primary button1">Thêm</button>
                                </div>
                                <div class="form-group col-sm-3">
                                    <button type="submit" class="btn-block btn-primary button1">Sửa</button>
                                </div>
                                <div class="form-group col-sm-3">
                                    <button type="submit" class="btn-block btn-primary button1">Xóa</button>
                                </div>
                                <div class="form-group col-sm-3">
                                    <button type="submit" class="btn-block btn-primary button1">Làm mới</button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </body>

    </html>