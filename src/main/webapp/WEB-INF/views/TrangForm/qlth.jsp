<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <!DOCTYPE html>
        <html>

        <head>
            <meta charset="utf-8">
            <title>Insert title here</title>
            <link rel="stylesheet" href="../css/admin.css">
            <link href="img/favicon.ico" rel="icon">

            <!-- Google Web Fonts -->
            <link rel="preconnect" href="https://fonts.gstatic.com">
            <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">

            <!-- Font Awesome -->
            <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">

            <!-- Libraries Stylesheet -->
            <link href="../lib/animate/animate.min.css" rel="stylesheet">
            <link href="../lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
            <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.9.2/dist/umd/popper.min.js"
                integrity="sha384-IQsoLXl5PILFhosVNubq5LC7Qb9DXgDA9i+tQ8Zj3iwWAwPtgFTxbJ8NT4GN1R8p"
                crossorigin="anonymous"></script>
            <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.min.js"
                integrity="sha384-cVKIPhGWiC2Al4u+LWgxfKTRIcfu0JTxR+EQDz/bgldoEyl4H0zUF0QKbrJ0EcQF"
                crossorigin="anonymous"></script>
        </head>

        <body>
            <div id="wrapper">
                <h1>Bảng quản lý thương hiệu</h1>
                <div class="tablecuon" style="overflow-x: scroll; max-width: 100%;">
                    <table id="keywords" class="table tavle-border">
                        <thead class="">
                            <tr>
                                <th>EDIT</th>
                                <th><span>Mã Thương Hiệu</span></th>
                                <th><span>Tên Thương Hiệu</span></th>
                                <th><span>Logo</span></th>

                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="th" items="${th2.content}">
                                <tr>
                                    <th><a href="/trangchuad/2/edit/${th.thid}"
                                            class="text-danger text-decoration-none">EDIT</a></th>
                                    <td>${th.thid}</td>
                                    <td>${th.tenth}</td>
                                    <td>${th.logo}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
                <div class="text-center my-3">
                    <a class="p-4" href="/trangchuad/2?p=0"><button class="button1">First</button></a>
                    <a class="p-4" href="/trangchuad/2?p=${th2.number-1}"><button class="button1">Previous</button></a>
                    <a class="p-4" href="/trangchuad/2?p=${th2.number+1}"><button class="button1">Next</button></a>
                    <a class="p-4" href="/trangchuad/2?p=${th2.totalPages-1}"><button class="button1">Last</button></a>
                </div>
            </div>
        </body>

        </html>