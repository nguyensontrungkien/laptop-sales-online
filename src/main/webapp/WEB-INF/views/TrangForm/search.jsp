<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
            <!DOCTYPE html>
            <html>

            <head>
                <meta charset="utf-8">
                <title>Insert title here</title>
                <script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>

                <!-- <script>
                    $(document).ready(function () {
                        // Xử lý sự kiện khi checkbox thay đổi trạng thái
                        $('input[name="location"]').on('click', function () {
                            // Lấy danh sách các checkbox đã chọn
                            var selectedLocations = [];
                            $('input[name="location"]:checked').each(function () {
                                selectedLocations.push($(this).val());
                            });

                            // Gửi dữ liệu lên server bằng AJAX
                            $.ajax({
                                url: '/findsearch',
                                type: 'GET',
                                data: { locations: selectedLocations },
                                success: function (response) {
                                    // Xử lý kết quả tìm kiếm trả về từ server
                                    // ...
                                },
                                error: function () {
                                    // Xử lý khi có lỗi xảy ra trong quá trình gửi AJAX
                                    // ...
                                }
                            });
                        });
                    });
                </script> -->
            </head>

            <body>
                <!-- Breadcrumb Start -->
                <div class="container-fluid">
                    <div class="row px-xl-5">
                        <div class="col-12">
                            <nav class="breadcrumb bg-light mb-30">
                                <a class="breadcrumb-item text-dark" href="#">Home</a>
                                <a class="breadcrumb-item text-dark" href="#">Shop</a>
                                <span class="breadcrumb-item active">Shop List</span>
                            </nav>
                        </div>
                    </div>
                </div>
                <!-- Breadcrumb End -->


                <!-- Shop Start -->
                <div class="container-fluid">
                    <div class="row px-xl-5">
                        <!-- Shop Sidebar Start -->

                        <div class="col-lg-3 col-md-4">
                            <form action="/search/findsearch/" method="post">
                                <!-- Price Start -->

                                <!-- Price End -->

                                <!-- Color Start -->
                                <h5 class="section-title position-relative text-uppercase mb-3"><span
                                        class="bg-secondary pr-3">Filter by Category</span></h5>
                                <div class="bg-light p-4 mb-30">
                                    <!-- <form> -->
                                    <div
                                        class="custom-control custom-checkbox d-flex align-items-center justify-content-between mb-3">
                                        <input type="checkbox" class="custom-control-input" id="color-all">
                                        <label class="custom-control-label" for="price-all">All Color</label>
                                        <span class="badge border font-weight-normal">1000</span>
                                    </div>
                                    <div
                                        class="custom-control custom-checkbox d-flex align-items-center justify-content-between mb-3">
                                        <input type="checkbox" class="custom-control-input" id="color-1" name="th"
                                            value="ACER">
                                        <label class="custom-control-label" for="color-1">Acer</label>
                                        <span class="badge border font-weight-normal">150</span>
                                    </div>
                                    <div
                                        class="custom-control custom-checkbox d-flex align-items-center justify-content-between mb-3">
                                        <input type="checkbox" class="custom-control-input" id="color-2" name="th"
                                            value="ASUS">
                                        <label class="custom-control-label" for="color-2">Asus</label>
                                        <span class="badge border font-weight-normal">295</span>
                                    </div>
                                    <div
                                        class="custom-control custom-checkbox d-flex align-items-center justify-content-between mb-3">
                                        <input type="checkbox" class="custom-control-input" id="color-3" name="th"
                                            value="LENOVO">
                                        <label class="custom-control-label" for="color-3">Lenovo</label>
                                        <span class="badge border font-weight-normal">246</span>
                                    </div>
                                    <div
                                        class="custom-control custom-checkbox d-flex align-items-center justify-content-between mb-3">
                                        <input type="checkbox" class="custom-control-input" id="color-4">
                                        <label class="custom-control-label" for="color-4">Blue</label>
                                        <span class="badge border font-weight-normal">145</span>
                                    </div>
                                    <div
                                        class="custom-control custom-checkbox d-flex align-items-center justify-content-between">
                                        <input type="checkbox" class="custom-control-input" id="color-5">
                                        <label class="custom-control-label" for="color-5">Green</label>
                                        <span class="badge border font-weight-normal">168</span>
                                    </div>
                                    <!-- </form> -->
                                </div>
                                <!-- Color End -->

                                <!-- Size Start -->
                                <h5 class="section-title position-relative text-uppercase mb-3"><span
                                        class="bg-secondary pr-3">Filter by Color</span></h5>
                                <div class="bg-light p-4 mb-30">
                                    <!-- <form> -->
                                    <div
                                        class="custom-control custom-checkbox d-flex align-items-center justify-content-between mb-3">
                                        <input type="checkbox" class="custom-control-input" checked id="size-all">
                                        <label class="custom-control-label" for="size-all">All Size</label>
                                        <span class="badge border font-weight-normal">1000</span>
                                    </div>
                                    <div
                                        class="custom-control custom-checkbox d-flex align-items-center justify-content-between mb-3">
                                        <input type="checkbox" class="custom-control-input" id="size-1" name="mau"
                                            value="Đen">
                                        <label class="custom-control-label" for="size-1">Đen</label>
                                        <span class="badge border font-weight-normal">150</span>
                                    </div>
                                    <div
                                        class="custom-control custom-checkbox d-flex align-items-center justify-content-between mb-3">
                                        <input type="checkbox" class="custom-control-input" id="size-2" name="mau"
                                            value="Xám">
                                        <label class="custom-control-label" for="size-2">Xám</label>
                                        <span class="badge border font-weight-normal">295</span>
                                    </div>
                                    <div
                                        class="custom-control custom-checkbox d-flex align-items-center justify-content-between mb-3">
                                        <input type="checkbox" class="custom-control-input" id="size-3">
                                        <label class="custom-control-label" for="size-3">M</label>
                                        <span class="badge border font-weight-normal">246</span>
                                    </div>
                                    <div
                                        class="custom-control custom-checkbox d-flex align-items-center justify-content-between mb-3">
                                        <input type="checkbox" class="custom-control-input" id="size-4">
                                        <label class="custom-control-label" for="size-4">L</label>
                                        <span class="badge border font-weight-normal">145</span>
                                    </div>
                                    <div
                                        class="custom-control custom-checkbox d-flex align-items-center justify-content-between">
                                        <input type="checkbox" class="custom-control-input" id="size-5">
                                        <label class="custom-control-label" for="size-5">XL</label>
                                        <span class="badge border font-weight-normal">168</span>
                                    </div>
                                    <!-- </form> -->
                                </div>
                                <!-- Size End -->
                                <button formaction="/search/findsearch/" type="submit"
                                    class="btn btn-info">SEARCH</button>
                            </form>
                        </div>


                        <!-- Shop Sidebar End -->


                        <!-- Shop Product Start -->
                        <div class="col-lg-9 col-md-8">
                            <div class="row pb-3">
                                <div class="col-12 pb-1">
                                    <div class="d-flex align-items-center justify-content-between mb-4">
                                        <div>
                                            <button class="btn btn-sm btn-light"><i class="fa fa-th-large"></i></button>
                                            <button class="btn btn-sm btn-light ml-2"><i
                                                    class="fa fa-bars"></i></button>
                                        </div>
                                        <div class="ml-2">
                                            <div class="btn-group">
                                                <button type="button" class="btn btn-sm btn-light dropdown-toggle"
                                                    data-toggle="dropdown">Sorting</button>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="#">Latest</a>
                                                    <a class="dropdown-item" href="#">Popularity</a>
                                                    <a class="dropdown-item" href="#">Best Rating</a>
                                                </div>
                                            </div>
                                            <div class="btn-group ml-2">
                                                <button type="button" class="btn btn-sm btn-light dropdown-toggle"
                                                    data-toggle="dropdown">Showing</button>
                                                <div class="dropdown-menu dropdown-menu-right">
                                                    <a class="dropdown-item" href="#">10</a>
                                                    <a class="dropdown-item" href="#">20</a>
                                                    <a class="dropdown-item" href="#">30</a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <c:forEach var="sp" items="${sp}">
                                    <div class="col-lg-4 col-md-6 col-sm-6 pb-1">

                                        <div class="product-item bg-light mb-4">
                                            <div class="product-img position-relative overflow-hidden">
                                                <img class="img-fluid w-100" src="../images/${sp.hinh}" alt="">
                                                <div class="product-action">
                                                    <a class="btn btn-outline-dark btn-square" href=""><i
                                                            class="fa fa-shopping-cart"></i></a>
                                                    <a class="btn btn-outline-dark btn-square" href=""><i
                                                            class="far fa-heart"></i></a>
                                                    <a class="btn btn-outline-dark btn-square" href=""><i
                                                            class="fa fa-sync-alt"></i></a>
                                                    <a class="btn btn-outline-dark btn-square" href=""><i
                                                            class="fa fa-search"></i></a>
                                                </div>
                                            </div>
                                            <div class="text-center py-4">
                                                <a class="h6 text-decoration-none text-truncate" href="">${sp.tensp}</a>
                                                <div class="d-flex align-items-center justify-content-center mt-2">
                                                    <h5>$123.00</h5>
                                                    <h6 class="text-muted ml-2"><del>$123.00</del></h6>
                                                </div>
                                                <div class="d-flex align-items-center justify-content-center mb-1">
                                                    <small class="fa fa-star text-primary mr-1"></small>
                                                    <small class="fa fa-star text-primary mr-1"></small>
                                                    <small class="fa fa-star text-primary mr-1"></small>
                                                    <small class="fa fa-star text-primary mr-1"></small>
                                                    <small class="fa fa-star text-primary mr-1"></small>
                                                    <small>(99)</small>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                </c:forEach>
                                <!-- <nav>
                                        <ul class="pagination justify-content-center">
                                            <li class="page-item disabled"><a class="page-link"
                                                    href="#">Previous</span></a>
                                            </li>
                                            <li class="page-item active"><a class="page-link" href="#">1</a></li>
                                            <li class="page-item"><a class="page-link" href="#">2</a></li>
                                            <li class="page-item"><a class="page-link" href="#">3</a></li>
                                            <li class="page-item"><a class="page-link" href="#">Next</a></li>
                                        </ul>
                                    </nav> -->
                            </div>
                        </div>
                    </div>
                    <!-- Shop Product End -->
                </div>
                </div>
                <!-- Shop End -->
            </body>

            </html>