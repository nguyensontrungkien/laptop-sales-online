<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <!DOCTYPE html>
        <html>

        <head>
            <meta charset="utf-8">
            <title>Insert title here</title>
            <link rel="stylesheet" href="../css/qlsp.css">
            <link rel="stylesheet" href="../css/admin.css">
        </head>

        <body>
            <div class="container-fluid">
                <header class="text-center">
                    <h1>ĐƠN HÀNG ĐANG XỬ LÝ CỦA BẠN</h1>
                </header>
                <div class="row">
                    <div class="col-8">
                        <div class="border rounded p-4"
                            style="background: linear-gradient(to bottom, #000428, #004683);">
                            <c:forEach var="dh" items="${dh.content}">
                                <a href="/ctdh/${dh.dhid}" class="text-decoration-none text-dark font-weight-bolder">
                                    <card class="card">
                                        <div class="card-body">
                                            <div class="card-title">Đơn hàng</div>
                                            <div class="">Ngày đặt : ${dh.ngaydat}</div>
                                            <div class="">Tổng giá : ${dh.tongtien}</div>
                                        </div>
                                    </card>
                                </a>
                            </c:forEach>
                            <div class="text-center my-3">
                                <a class="p-4" href="/ctdh/dangxuli/?p=0"><button
                                        class="button1 btn-info">First</button></a>
                                <a class="p-4" href="/ctdh/dangxuli/?p=${dh.number-1}"><button
                                        class="button1 btn-info">Previous</button></a>
                                <a class="p-4" href="/ctdh/dangxuli/?p=${dh.number+1}"><button
                                        class="button1 btn-info">Next</button></a>
                                <a class="p-4" href="/ctdh/dangxuli/?p=${dh.totalPages-1}"><button
                                        class="button1 btn-info">Last</button></a>
                            </div>
                        </div>
                    </div>
                    <div class="col-4">
                        <div class="border rounded p-4"
                            style="background: linear-gradient(to bottom, #000428, #004683);">
                            <card class="card" style="height: auto;">
                                <h2>Các sản phẩm trong đơn hàng ${check}</h2>
                                <h4>Số lượng: ${count}</h4>
                                <c:forEach var="dhsp" items="${dhsp}">
                                    <card class="card">
                                        <h3>${dhsp.sanpham.tensp}</h3>
                                        <p class="text-dark py-2 my-1"><span>Màu sắc: </span>${dhsp.sanpham.mausac}</p>
                                        <p class="text-dark py-2 my-1"><span>Thương hiệu:
                                            </span>${dhsp.sanpham.thuonghieu.thid}</p>
                                    </card>
                                </c:forEach>
                            </card>
                        </div>
                    </div>
                </div>
            </div>
        </body>

        </html>