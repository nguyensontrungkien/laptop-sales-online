<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <!DOCTYPE html>
        <html>

        <head>
            <meta charset="utf-8">
            <title>Insert title here</title>
            <link rel="stylesheet" href="../css/admin.css">
            <link href="img/favicon.ico" rel="icon">

            <!-- Google Web Fonts -->
            <link rel="preconnect" href="https://fonts.gstatic.com">
            <link href="https://fonts.googleapis.com/css2?family=Roboto:wght@400;500;700&display=swap" rel="stylesheet">

            <!-- Font Awesome -->
            <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.10.0/css/all.min.css" rel="stylesheet">

            <!-- Libraries Stylesheet -->
            <link href="../lib/animate/animate.min.css" rel="stylesheet">
            <link href="../lib/owlcarousel/assets/owl.carousel.min.css" rel="stylesheet">
        </head>

        <body>
            <div id="wrapper">
                <h1>Bảng quản lý sản phẩm</h1>
                <div class="tablecuon" style="overflow-x: scroll; max-width: 100%;">
                    <table id="keywords" class="table">
                        <thead>
                            <tr>
                                <th>EDIT</th>
                                <th><span>Tên Sản Phẩm</span></th>
                                <th><span>Thương Hiệu</span></th>
                                <th><span>Giá</span></th>
                                <th><span>Giá Cũ</span></th>
                                <th><span>Hình</span></th>
                                <th><span>Màu Sắc</span></th>
                                <th><span>CPU</span></th>
                                <th><span>Số Nhân</span></th>
                                <th><span>Số Luồng</span></th>
                                <th><span>Tốc Độ CPU</span></th>
                                <th><span>Tốc Độ Tối Đa</span></th>
                                <th><span>Ram</span></th>
                                <th><span>Loại Ram</span></th>
                                <th><span>Tốc Độ Bus Ram</span></th>
                                <th><span>Hỗ Trợ Ram Tối Đa</span></th>
                                <th><span>Rom</span></th>
                                <th><span>Màn Hình</span></th>
                                <th><span>Độ Phân Giải</span></th>
                                <th><span>Tần Số Quét</span></th>
                                <th><span>Công Nghệ Màn Hình</span></th>
                                <th><span>Card Màn Hình</span></th>
                                <th><span>Công Nghệ Âm Thanh</span></th>
                                <th><span>Tính Năng Khác</span></th>
                                <th><span>Đèn Bàn Phím</span></th>
                                <th><span>Hệ Điều Hành</span></th>
                                <th><span>Thông Tin Pin</span></th>
                                <th><span>Thiết Kế</span></th>
                                <th><span>Ngày Ra Mắt</span></th>
                                <th><span>Kích Thước</span></th>
                            </tr>
                        </thead>
                        <tbody>
                            <c:forEach var="sp" items="${sp.content}">
                                <tr>
                                    <th><a href="" class="text-danger text-decoration-none">EDIT</a></th>
                                    <td>${sp.tensp}</td>
                                    <td>${sp.thuonghieu.tenth}</td>
                                    <td>${sp.gia}</td>
                                    <td>${sp.giacu}</td>
                                    <td>${sp.hinh}</td>
                                    <td>${sp.mausac}</td>
                                    <td>${sp.cpu}</td>
                                    <td>${sp.sonhan}</td>
                                    <td>${sp.soluong}</td>
                                    <td>${sp.tocdocpu}</td>
                                    <td>${sp.tocdotoida}</td>
                                    <td>${sp.ram}</td>
                                    <td>${sp.loairam}</td>
                                    <td>${sp.tocdobusram}</td>
                                    <td>${sp.hotroramtoida}</td>
                                    <td>${sp.rom}</td>
                                    <td>${sp.manhinh}</td>
                                    <td>${sp.dophangiai}</td>
                                    <td>${sp.tansoquet}</td>
                                    <td>${sp.congnghemanhinh}</td>
                                    <td>${sp.cardmh}</td>
                                    <td>${sp.congngheamthanh}</td>
                                    <td>${sp.tinhnangkhac}</td>
                                    <td>${sp.denbanphim}</td>
                                    <td>${sp.hdh}</td>
                                    <td>${sp.thongtinpin}</td>
                                    <td>${sp.thietke}</td>
                                    <td>${sp.ngaysx}</td>
                                    <td>${sp.thietke}</td>
                                </tr>
                            </c:forEach>
                        </tbody>
                    </table>
                </div>
                <div class="text-center my-3">
                    <a class="p-4" href="/trangchuad/1?p=0"><button class="button1">First</button></a>
                    <a class="p-4" href="/trangchuad/1?p=${sp.number-1}"><button class="button1">Previous</button></a>
                    <a class="p-4" href="/trangchuad/1?p=${sp.number+1}"><button class="button1">Next</button></a>
                    <a class="p-4" href="/trangchuad/1?p=${sp.totalPages-1}"><button class="button1">Last</button></a>
                </div>
            </div>
        </body>

        </html>