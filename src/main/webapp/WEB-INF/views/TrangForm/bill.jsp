<%@ page language="java" contentType="text/html; charset=utf-8" pageEncoding="utf-8" %>
    <%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
        <%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
            <!DOCTYPE html>
            <html>

            <head>
                <meta charset="utf-8">
                <title>Insert title here</title>
            </head>

            <body>
                <!-- Breadcrumb Start -->
                <div class="container-fluid">
                    <div class="row px-xl-5">
                        <div class="col-12">
                            <nav class="breadcrumb bg-light mb-30">
                                <a class="breadcrumb-item text-dark" href="#">Home</a>
                                <a class="breadcrumb-item text-dark" href="#">Shop</a>
                                <span class="breadcrumb-item active">Checkout</span>
                            </nav>
                        </div>
                    </div>
                </div>
                <!-- Breadcrumb End -->
                <!-- Checkout Start -->
                <form action="/donhang/ok" method="get">
                    <div class="container-fluid">
                        <div class="row px-xl-5">
                            <div class="col-lg-7">
                                <h5 class="section-title position-relative text-uppercase mb-3"><span
                                        class="bg-secondary pr-3">Billing
                                        Address</span></h5>
                                <div class="bg-light p-30 mb-5">
                                    <div class="row">
                                        <div class="col-md-6 form-group">
                                            <label>Họ Và Tên</label>
                                            <input class="form-control" type="text" placeholder="John"
                                                value="${gh.get(0).getUsers().getUsername()}">
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Địa Chỉ</label>
                                            <input class="form-control" type="text" placeholder="Doe"
                                                value="${gh.get(0).getUsers().getDiachi()}" path="body" name="body">
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Email</label>
                                            <input class="form-control" type="text" placeholder="example@email.com"
                                                value="${gh.get(0).getUsers().getEmail()}" path="to" name="to">
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Số Điện Thoại</label>
                                            <input class="form-control" type="text" placeholder="+123 456 789"
                                                value="${gh.get(0).getUsers().getSdt()}" path="subject" name="subject">
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Thành Phố</label>
                                            <select class="custom-select">
                                                <option selected>HCM</option>
                                                <option>Hà Nội</option>
                                                <option>Albania</option>
                                                <option>Algeria</option>
                                            </select>
                                        </div>
                                        <div class="col-md-6 form-group">
                                            <label>Ngày Đặt Hàng</label>
                                            <input class="form-control" type="text" placeholder="" value="${ngaydat}">
                                        </div>
                                        <div class="col-md-12 form-group">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="newaccount">
                                                <label class="custom-control-label" for="newaccount">Create an
                                                    account</label>
                                            </div>
                                        </div>
                                        <div class="col-md-12">
                                            <div class="custom-control custom-checkbox">
                                                <input type="checkbox" class="custom-control-input" id="shipto">
                                                <label class="custom-control-label" for="shipto" data-toggle="collapse"
                                                    data-target="#shipping-address">Ship to different address</label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="mb-5">
                                    <h5 class="section-title position-relative text-uppercase mb-3"><span
                                            class="bg-secondary pr-3">Payment</span></h5>
                                    <div class="bg-light p-30">
                                        <div class="form-group">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" name="payment"
                                                    id="paypal">
                                                <label class="custom-control-label" for="paypal">Paypal</label>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" name="payment"
                                                    id="directcheck">
                                                <label class="custom-control-label" for="directcheck">Direct
                                                    Check</label>
                                            </div>
                                        </div>
                                        <div class="form-group mb-4">
                                            <div class="custom-control custom-radio">
                                                <input type="radio" class="custom-control-input" name="payment"
                                                    id="banktransfer">
                                                <label class="custom-control-label" for="banktransfer">Bank
                                                    Transfer</label>
                                            </div>
                                        </div>

                                        <button formaction="/donhang/ok/"
                                            class="btn btn-block btn-primary font-weight-bold py-3">Place
                                            Order</button>
                                    </div>
                                </div>
                            </div>
                            <div class="col-lg-5">
                                <h5 class="section-title position-relative text-uppercase mb-3"><span
                                        class="bg-secondary pr-3">Order
                                        Total</span></h5>
                                <div class="bg-light p-30 mb-5">
                                    <div class="border-bottom">
                                        <h6 class="mb-3">Sản Phẩm</h6>
                                        <c:forEach var="gh" items="${gh}">
                                            <div class="card rounded py-3 mb-3 px-2"
                                                style="background: linear-gradient(to bottom, #b0d8ce, #fcfdff);">
                                                <div class=" d-flex justify-content-between text-dark">
                                                    <p class="font-weight-bold" style="color: blue;">${gh.sanpham.tensp}
                                                    </p>
                                                    <p class="text-danger">
                                                        <fmt:setLocale value="vi_VN" />
                                                        <fmt:formatNumber value="${gh.sanpham.gia * gh.soluong}"
                                                            type="number" pattern="#,##0" /><span> đ</span>
                                                    </p>
                                                </div>
                                            </div>
                                        </c:forEach>
                                    </div>
                                    <div class="border-bottom pt-3 pb-2">
                                        <div class="d-flex justify-content-between mb-3">
                                            <h6>Subtotal</h6>
                                            <h6>30.000 đ</h6>
                                        </div>
                                        <div class="d-flex justify-content-between">
                                            <h6 class="font-weight-medium">Shipping</h6>
                                            <h6 class="font-weight-medium">50.000 đ</h6>
                                        </div>
                                    </div>
                                    <div class="pt-2">
                                        <div class="d-flex justify-content-between mt-2">
                                            <h5>Total</h5>
                                            <h5 class="text-danger">${total}</h5>
                                        </div>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                    <!-- Checkout End -->
                </form>
            </body>

            </html>